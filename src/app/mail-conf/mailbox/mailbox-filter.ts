import { Component } from '@angular/core';
import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

const DATA = [
  { service_name: '구와아아악', begin_time: new Date, end_time: new Date, create_time: new Date },
  { service_name: '구와아아악', begin_time: new Date, end_time: new Date, create_time: new Date },
  { service_name: '구와아아악', begin_time: new Date, end_time: new Date, create_time: new Date },
  { service_name: '구와아아악', begin_time: new Date, end_time: new Date, create_time: new Date }
];

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mail-conf-mailbox-filter',
  templateUrl: './mailbox-filter.html',
  styleUrls: ['./mailbox-filter.scss'],
  preserveWhitespaces: false
})
// tslint:disable-next-line:component-class-suffix
export class MailConfMailboxFilter {
  dataSource: AbsentDataSource | null;
  displayedColumns = ['checked', 'rule', 'savedMailbox', 'etc', 'change', 'delete'];

  constructor() {
    this.dataSource = new AbsentDataSource(DATA);
  }
}

export class AbsentDataSource extends DataSource<any> {

  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  get data(): any[] { return this.dataChange.value; }

  constructor(absent_list: any[]) {
    super();
    this.dataChange.next(absent_list);
  }

  connect(): Observable<any[]> {

    return this.dataChange;
  }

  disconnect() {

  }

  getData(): any[] {
    return this.data;
  }

  addAbsent(bean: any) {

    this.data.push(bean);
    this.dataChange.next(this.data);
  }

  deleteAbsent(sn: number) {

    let delete_idx = -1;
    for (let i = 0; i < this.data.length; i++) {
      if (this.data[i].sn === sn) {
        delete_idx = i;
        break;
      }
    }
    if (delete_idx !== -1) {
      this.data.splice(delete_idx, 1);
    }

    this.dataChange.next(this.data);

    return delete_idx !== -1;
  }
}
