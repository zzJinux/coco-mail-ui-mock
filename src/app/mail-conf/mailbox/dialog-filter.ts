import { Component } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'dialog-filter',
  templateUrl: './dialog-filter.html',
  styleUrls: ['./dialog-filter.scss'],
  preserveWhitespaces: false
})
// tslint:disable-next-line:component-class-suffix
export class DialogFilter {
  pms = {};

  saveReady() {
    return false;
  }
}
