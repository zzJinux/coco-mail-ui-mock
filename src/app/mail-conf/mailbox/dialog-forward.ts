import { Component } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'dialog-forward',
  templateUrl: './dialog-forward.html',
  styleUrls: ['./dialog-forward.scss'],
  preserveWhitespaces: false
})
// tslint:disable-next-line:component-class-suffix
export class DialogForward {
  pms = {};

  saveReady() {
    return false;
  }
}
