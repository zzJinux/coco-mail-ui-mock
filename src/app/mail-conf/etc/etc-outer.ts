import { Component } from '@angular/core';
import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

const DATA = [
  { service_name: '구와아아악', account: 'sdfsdfsdfss' },
  { service_name: '구와아아악', account: 'sdfsdfsdfsff' },
  { service_name: '구와아아악', account: 'sdfsdfsdfsf' },
  { service_name: '구와아아악', account: 'sdfsdfssdf' }
];

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mail-conf-etc-outer',
  templateUrl: './etc-outer.html',
  styleUrls: ['./etc-outer.scss'],
  preserveWhitespaces: false
})
// tslint:disable-next-line:component-class-suffix
export class MailConfEtcOuter {
  dataSource: AbsentDataSource | null;
  displayedColumns = ['checked', 'serviceName', 'account', 'savedMailbox', 'saveOriginal', 'change', 'delete'];

  constructor() {
    this.dataSource = new AbsentDataSource(DATA);
  }
}

export class AbsentDataSource extends DataSource<any> {

  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  get data(): any[] { return this.dataChange.value; }

  constructor(absent_list: any[]) {
    super();
    this.dataChange.next(absent_list);
  }

  connect(): Observable<any[]> {

    return this.dataChange;
  }

  disconnect() {

  }

  getData(): any[] {
    return this.data;
  }

  addAbsent(bean: any) {

    this.data.push(bean);
    this.dataChange.next(this.data);
  }

  deleteAbsent(sn: number) {

    let delete_idx = -1;
    for (let i = 0; i < this.data.length; i++) {
      if (this.data[i].sn === sn) {
        delete_idx = i;
        break;
      }
    }
    if (delete_idx !== -1) {
      this.data.splice(delete_idx, 1);
    }

    this.dataChange.next(this.data);

    return delete_idx !== -1;
  }
}
