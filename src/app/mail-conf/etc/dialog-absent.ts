import { Component, ViewChild } from '@angular/core';
import { CdkPortal } from '@angular/cdk/portal';
import { Overlay, CdkOverlayOrigin } from '@angular/cdk/overlay';
import { OverlayConfig } from '@angular/cdk/overlay';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'dialog-absent',
  templateUrl: './dialog-absent.html',
  styleUrls: ['./dialog-absent.scss'],
  preserveWhitespaces: false
})
// tslint:disable-next-line:component-class-suffix
export class DialogAbsent {

  @ViewChild('contactsTemplate') cdkPortal: CdkPortal;
  @ViewChild(CdkOverlayOrigin) cdkOverlayOrigin: CdkOverlayOrigin;

  other_list = [
    { from_email: 'sookwook2@naver.com' },
    { from_email: 'sookwook2@naver.com' },
    { from_email: 'sookwook2@naver.com' },
    { from_email: 'sookwook2@naver.com' },
    { from_email: 'sookwook2@naver.com' },
    { from_email: 'sookwook2@naver.com' },
    { from_email: 'sookwook2@naver.com' },
    { from_email: 'sookwook2@naver.com' }
  ];

  constructor(public overlay: Overlay) {}

  openContacts() {
    const config = new OverlayConfig();
    config.positionStrategy = this.overlay.position().connectedTo(
      this.cdkOverlayOrigin.elementRef,
      { originX: 'start', originY: 'bottom' },
      { overlayX: 'start', overlayY: 'top' });

    const overlayRef = this.overlay.create(config);

    overlayRef.attach(this.cdkPortal);
  }
}
