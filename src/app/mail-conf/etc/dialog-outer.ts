import { Component } from '@angular/core';
import { Validators, FormControl, FormGroup } from '@angular/forms';

const ID_REGEX = /^[a-zA-Z0-9_+.-]{2,30}$/;
const DOMAIN_REGEX = /^([a-zA-Z0-9-]+\.)+[a-zA-Z0-9]{2,4}$/;
const PORT_REGEX = /^[0-9]{3,5}$/;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'dialog-outer',
  templateUrl: './dialog-outer.html',
  styleUrls: ['./dialog-outer.scss'],
  preserveWhitespaces: false
})
// tslint:disable-next-line:component-class-suffix
export class DialogOuter {

  outerForm: FormGroup;

  title = '저저저거거거거서서서';
  accountFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(ID_REGEX)]);

  _css = {
    session: {
      domain_list: [
        'asdfsdfas', 's;jlkkj;j', 'werwerty'
      ]
    }
  };

  pms = {};

  get service() { return this.outerForm.get('service'); }
  get server() { return this.outerForm.get('server'); }
  get account() { return this.outerForm.get('account'); }
  get password() { return this.outerForm.get('password'); }
  get port() { return this.outerForm.get('port'); }

  constructor() {
    this.outerForm = new FormGroup({
      service: new FormControl('', Validators.required),
      server: new FormControl('', [Validators.required, Validators.pattern(DOMAIN_REGEX)]),
      account: new FormControl('', [Validators.required, Validators.pattern(ID_REGEX)]),
      password: new FormControl('', Validators.required),
      port: new FormControl('110', [Validators.required, Validators.pattern(PORT_REGEX)])
    });
  }

  saveReady() {
    return false;
  }
}
