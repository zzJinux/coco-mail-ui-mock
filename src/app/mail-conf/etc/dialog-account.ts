import { Component } from '@angular/core';
import { Validators, FormControl } from '@angular/forms';

const ID_REGEX = /^[a-zA-Z0-9_+.-]{2,30}$/;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'dialog-account',
  templateUrl: './dialog-account.html',
  styleUrls: ['./dialog-account.scss'],
  preserveWhitespaces: false
})
// tslint:disable-next-line:component-class-suffix
export class DialogAccount {
  accountFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(ID_REGEX)]);

  _css = {
    session: {
      domain_list: [
        'asdfsdfas', 's;jlkkj;j', 'werwerty'
      ]
    }
  };
}
