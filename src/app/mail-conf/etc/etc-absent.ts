import { Component } from '@angular/core';
import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

const DATA = [
  {sn: 1, begin_time: new Date, end_time: new Date, create_time: new Date, subject: '가갸거겨고교구규고고고고고곤잘레스'},
  {sn: 1, begin_time: new Date, end_time: new Date, create_time: new Date, subject: '가갸거겨고교구규고고고고고곤잘레스'},
  {sn: 1, begin_time: new Date, end_time: new Date, create_time: new Date, subject: '가갸거겨고교구규고고고고고곤잘레스'},
  {sn: 1, begin_time: new Date, end_time: new Date, create_time: new Date, subject: '가갸거겨고교구규고고고고고곤잘레스'},
  {sn: 1, begin_time: new Date, end_time: new Date, create_time: new Date, subject: '가갸거겨고교구규고고고고고곤잘레스'},
  {sn: 1, begin_time: new Date, end_time: new Date, create_time: new Date, subject: '가갸거겨고교구규고고고고고곤잘레스'},
  {sn: 1, begin_time: new Date, end_time: new Date, create_time: new Date, subject: '가갸거겨고교구규고고고고고곤잘레스'},
  {sn: 1, begin_time: new Date, end_time: new Date, create_time: new Date, subject: '가갸거겨고교구규고고고고고곤잘레스'},
  {sn: 1, begin_time: new Date, end_time: new Date, create_time: new Date, subject: '가갸거겨고교구규고고고고고곤잘레스'},
  {sn: 1, begin_time: new Date, end_time: new Date, create_time: new Date, subject: '가갸거겨고교구규고고고고고곤잘레스'},
  {sn: 1, begin_time: new Date, end_time: new Date, create_time: new Date, subject: '가갸거겨고교구규고고고고고곤잘레스'},
  {sn: 1, begin_time: new Date, end_time: new Date, create_time: new Date, subject: '가갸거겨고교구규고고고고고곤잘레스'},
  {sn: 1, begin_time: new Date, end_time: new Date, create_time: new Date, subject: '가갸거겨고교구규고고고고고곤잘레스'},
  {sn: 1, begin_time: new Date, end_time: new Date, create_time: new Date, subject: '가갸거겨고교구규고고고고고곤잘레스'},
  {sn: 1, begin_time: new Date, end_time: new Date, create_time: new Date, subject: '가갸거겨고교구규고고고고고곤잘레스'},
  {sn: 1, begin_time: new Date, end_time: new Date, create_time: new Date, subject: '가갸거겨고교구규고고고고고곤잘레스'},
  {sn: 1, begin_time: new Date, end_time: new Date, create_time: new Date, subject: '가갸거겨고교구규고고고고고곤잘레스'},
  {sn: 1, begin_time: new Date, end_time: new Date, create_time: new Date, subject: '가갸거겨고교구규고고고고고곤잘레스'},
  {sn: 1, begin_time: new Date, end_time: new Date, create_time: new Date, subject: '가갸거겨고교구규고고고고고곤잘레스'},
  {sn: 1, begin_time: new Date, end_time: new Date, create_time: new Date, subject: '가갸거겨고교구규고고고고고곤잘레스'}
];

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mail-conf-etc-absent',
  templateUrl: './etc-absent.html',
  styleUrls: ['./etc-absent.scss'],
  preserveWhitespaces: false
})
// tslint:disable-next-line:component-class-suffix
export class MailConfEtcAbsent {
  dataSource: AbsentDataSource | null;
  displayedColumns = ['selected', 'term', 'registerDate', 'change', 'delete'];

  constructor() {
    this.dataSource = new AbsentDataSource(DATA);
  }
}

export class AbsentDataSource extends DataSource<any> {

  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  get data(): any[] { return this.dataChange.value; }

  constructor(absent_list: any[]) {
    super();
    this.dataChange.next(absent_list);
  }

  connect(): Observable<any[]> {

    return this.dataChange;
  }

  disconnect() {

  }

  getData(): any[] {
    return this.data;
  }

  addAbsent(bean: any) {

    this.data.push(bean);
    this.dataChange.next(this.data);
  }

  deleteAbsent(sn: number) {

    let delete_idx = -1;
    for (let i = 0; i < this.data.length; i++) {
      if (this.data[i].sn === sn) {
        delete_idx = i;
        break;
      }
    }
    if (delete_idx !== -1) {
      this.data.splice(delete_idx, 1);
    }

    this.dataChange.next(this.data);

    return delete_idx !== -1;
  }
}
