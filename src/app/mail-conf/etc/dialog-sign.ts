import { Component, ViewChild } from '@angular/core';
import { CdkPortal } from '@angular/cdk/portal';
import { Overlay, CdkOverlayOrigin } from '@angular/cdk/overlay';
import { OverlayConfig } from '@angular/cdk/overlay';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'dialog-sign',
  templateUrl: './dialog-sign.html',
  styleUrls: ['./dialog-sign.scss'],
  preserveWhitespaces: false
})
// tslint:disable-next-line:component-class-suffix
export class DialogSign {

  @ViewChild('contactsTemplate') cdkPortal: CdkPortal;
  @ViewChild(CdkOverlayOrigin) cdkOverlayOrigin: CdkOverlayOrigin;

  other_list = [
    { from_email: 'sookwook2@naver.com' },
    { from_email: 'sookwook2@naver.com' },
    { from_email: 'sookwook2@naver.com' },
    { from_email: 'sookwook2@naver.com' },
    { from_email: 'sookwook2@naver.com' },
    { from_email: 'sookwook2@naver.com' },
    { from_email: 'sookwook2@naver.com' },
    { from_email: 'sookwook2@naver.com' }
  ];

  constructor(public overlay: Overlay) {}
}
