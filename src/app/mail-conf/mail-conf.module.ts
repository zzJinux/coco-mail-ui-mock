import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MiddleUIModule } from '../middle-ui';
import { SharedModule } from '../shared.module';

import { MailConf } from './mail-conf';
import { MailConfTab } from './mail-conf-tab';
import { MailConfBasicList } from './basic/basic-list';
import { MailConfBasicDelete } from './basic/basic-delete';
import { MailConfBasicRead } from './basic/basic-read';
import { MailConfBasicWrite } from './basic/basic-write';
import { MailConfEtcAbsent } from './etc/etc-absent';
import { MailConfEtcSign } from './etc/etc-sign';
import { MailConfEtcOuter } from './etc/etc-outer';
import { MailConfEtcAccount } from './etc/etc-account';
import { MailConfMailboxFilter } from './mailbox/mailbox-filter';
import { MailConfMailboxForward } from './mailbox/mailbox-forward';
import { MailConfMailboxManage } from './mailbox/mailbox-manage';
import { DialogAbsent } from './etc/dialog-absent';
import { DialogAccount } from './etc/dialog-account';
import { DialogOuter } from './etc/dialog-outer';
import { DialogSign } from './etc/dialog-sign';
import { DialogFilter } from './mailbox/dialog-filter';
import { DialogForward } from './mailbox/dialog-forward';
import { MailConfSpamConfig } from './spam/spam-config';
import { MailConfSpamAllowDeny } from './spam/spam-allow-deny';

const ONLY_DECL = [
  MailConfBasicList,
  MailConfBasicDelete,
  MailConfBasicRead,
  MailConfBasicWrite,
  MailConfEtcAbsent,
  MailConfEtcSign,
  MailConfEtcOuter,
  MailConfEtcAccount,
  MailConfMailboxFilter,
  MailConfMailboxForward,
  MailConfMailboxManage,
  MailConfSpamConfig,
  MailConfSpamAllowDeny
];
const ENTRIES = [
  DialogAbsent,
  DialogAccount,
  DialogOuter,
  DialogSign,
  DialogFilter,
  DialogForward
];
const EXPORTS = [MailConf, MailConfTab];

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, RouterModule, MiddleUIModule, SharedModule],
  declarations: [ONLY_DECL, ENTRIES, EXPORTS],
  entryComponents: ENTRIES,
  exports: [ENTRIES, EXPORTS]
})
export class MailConfModule { }
