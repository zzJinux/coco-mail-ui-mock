import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mail-conf-tab',
  templateUrl: './mail-conf-tab.html',
  styleUrls: ['./mail-conf-tab.scss'],
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false
})
// tslint:disable-next-line:component-class-suffix
export class MailConfTab { }
