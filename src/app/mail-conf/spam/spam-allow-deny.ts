import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

const EMAIL_REGEX = /^[a-zA-Z0-9_+.-]{2,30}@([a-zA-Z0-9-]+\.)+[a-zA-Z0-9]{2,4}$/;
const DOMAIN_REGEX = /^([a-zA-Z0-9-]+\.)+[a-zA-Z0-9]{2,4}$/;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mail-conf-spam-allow-deny',
  templateUrl: './spam-allow-deny.html',
  styleUrls: ['./spam-allow-deny.scss'],
  preserveWhitespaces: false
})
// tslint:disable-next-line:component-class-suffix
export class MailConfSpamAllowDeny {
  deny_domain_list = [
    { domain: 'sddddddddddddddddddd' },
    { domain: 'sddddddddddddddddddd' },
    { domain: 'sddddddddddddddddddd' },
    { domain: 'sddddddddddddddddddd' }
  ];

  allowEmailControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(EMAIL_REGEX)
  ]);
  allowDomainControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(DOMAIN_REGEX)
  ]);
  denyEmailControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(EMAIL_REGEX)
  ]);
  denyDomainControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(DOMAIN_REGEX)
  ]);
}
