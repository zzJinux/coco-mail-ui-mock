import { Component } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mail-conf-spam-config',
  templateUrl: './spam-config.html',
  styleUrls: ['./spam-config.scss'],
  preserveWhitespaces: false
})
// tslint:disable-next-line:component-class-suffix
export class MailConfSpamConfig { }
