import { Component } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mail-conf-basic-write',
  templateUrl: './basic-write.html',
  styleUrls: ['./basic-write.scss'],
  preserveWhitespaces: false
})
// tslint:disable-next-line:component-class-suffix
export class MailConfBasicWrite {
  pms = {
    write_editor_mode: 'E'
  };
}
