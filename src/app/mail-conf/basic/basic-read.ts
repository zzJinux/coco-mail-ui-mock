import { Component } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mail-conf-basic-read',
  templateUrl: './basic-read.html',
  styleUrls: ['./basic-read.scss'],
  preserveWhitespaces: false
})
// tslint:disable-next-line:component-class-suffix
export class MailConfBasicRead {
  pms = {
    read_show_recipient: 'H',
    read_show_to: '0',
    read_show_cc: '0',
    read_show_bcc: '0',
    read_delete_after_menu: 'L',
    mail_content_print_mode: 'A'
  };
}
