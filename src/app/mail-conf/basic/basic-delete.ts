import { Component } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mail-conf-basic-delete',
  templateUrl: './basic-delete.html',
  styleUrls: ['./basic-delete.scss'],
  preserveWhitespaces: false
})
// tslint:disable-next-line:component-class-suffix
export class MailConfBasicDelete {
  pms = {
    receive_mailbox_delete_term: '0',
    personal_mailbox_delete_term: '0',
    send_mailbox_delete_term: '0',
    temp_mailbox_delete_term: '0',
    trash_delete_term: '0'
  };
}
