import { Component } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mail-conf-basic-list',
  templateUrl: './basic-list.html',
  styleUrls: ['./basic-list.scss'],
  preserveWhitespaces: false
})
// tslint:disable-next-line:component-class-suffix
export class MailConfBasicList {
  pms = {
    list_moreview_type: 'P',
    mailbox_page_size: '5',
    personal_mailbox_size: '0',
    content_preview: null,
    first_mailbox_menu: '9100',
    first_mailconf_menu: 'basic',
    first_mailconf_menu2: 'list',
    mailbox_refresh_term: '0',
    mailbox_basetime_gmt: '-11',
    mail_listing_type: null,
    newmail_arrive_alert: 'NOR',
    pcsave_filename_type: 'DFS'
  };
}
