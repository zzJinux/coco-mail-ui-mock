import { Component, ViewChild, Input, Output, HostListener, EventEmitter, ElementRef, ViewEncapsulation } from '@angular/core';
import { CdkPortal } from '@angular/cdk/portal';
import { OverlayRef, OverlayConfig, CdkOverlayOrigin, Overlay } from '@angular/cdk/overlay';

import { NODES_TEST as nodes } from '../mock-data/mailbox-tree';

@Component({
  selector: 'movecopy-popup',
  exportAs: 'movecopyPopup',
  templateUrl: './movecopy-popup.html',
  styleUrls: ['./movecopy-popup.css'],
  preserveWhitespaces: false
})
export class MovecopyPopup {

  nodes = nodes;

  @Input() useMove: boolean;
  @Input() useCopy: boolean;

  @Output() onMoveDone = new EventEmitter<number>();
  @Output() onCopyDone = new EventEmitter<number>();

  @ViewChild(CdkPortal) cdkPortal: CdkPortal;
  @Input() _origin: ElementRef;

  overlayRef: OverlayRef;

  constructor(private overlay: Overlay, private elementRef: ElementRef) { }

  openPopup(e: any) {
    if (this.overlayRef) {
      return;
    }

    const config = new OverlayConfig();
    config.positionStrategy = this.overlay.position().connectedTo(
      new ElementRef(this._origin),
      { originX: 'start', originY: 'bottom' },
      { overlayX: 'start', overlayY: 'top' });
    config.hasBackdrop = true;
    config.backdropClass = 'cdk-overlay-transparent-backdrop';

    this.overlayRef = this.overlay.create(config);
    this.overlayRef.attach(this.cdkPortal);
    this.overlayRef.backdropClick().subscribe(this.closePopup.bind(this));
  }

  closePopup() {
    if (this.overlayRef) {
      this.overlayRef.detach();
      this.overlayRef = null;
    }
  }
}
