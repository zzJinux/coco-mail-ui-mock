import { Component, ViewChild, HostListener, Input, ElementRef } from '@angular/core';
import { CdkPortal } from '@angular/cdk/portal';
import { OverlayRef, OverlayConfig, CdkOverlayOrigin, Overlay } from '@angular/cdk/overlay';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'tag-selector',
  exportAs: 'tagSelector',
  templateUrl: './tag-selector.html',
  styleUrls: ['./tag-selector.css'],
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    '[title]': 'tags',
  }
})
// tslint:disable-next-line:component-class-suffix
export class TagSelector {

  @Input() tags = '';
  @Input() mail_sn: number;

  @ViewChild(CdkPortal) cdkPortal: CdkPortal;
  @Input() _origin: ElementRef;
  overlayRef: OverlayRef;

  _css = {
    mailtag_list: [
      { name: 'shit', color: 'crimson', checked: false },
      { name: 'cdkcdk', color: 'green', checked: true }
    ]
  };

  constructor(private overlay: Overlay) { }

  setMailTag(e: Event) {
    // this.new_tagname = '';
    // this._css.mailtag_list.forEach(tag => {
    //   tag['checked'] = false;
    // });

    // if (this.tags) {
    //   const arr: string[] = this.tags.split(',');
    //   arr.forEach(v => {
    //     let t = -1;
    //     for (let i = 0; i < this._css.mailtag_list.length; i++) {
    //       if (this._css.mailtag_list[i].name === v) {
    //         t = i;
    //         break;
    //       }
    //     }

    //     if (t !== -1) { this._css.mailtag_list[t].checked = true; }
    //   });
    // }

    if (this.overlayRef) { this.closePopup(); }

    const config = new OverlayConfig();
    config.positionStrategy = this.overlay.position().connectedTo(
      new ElementRef(this._origin),
      { originX: 'start', originY: 'bottom' },
      { overlayX: 'end', overlayY: 'top' });
    config.hasBackdrop = true;
    config.backdropClass = 'cdk-overlay-transparent-backdrop';

    this.overlayRef = this.overlay.create(config);
    this.overlayRef.attach(this.cdkPortal);
    this.overlayRef.backdropClick().subscribe(this.closePopup.bind(this));

    // this.focusSetTagEmitter.emit(true);
  }

  closePopup() {
    if (this.overlayRef) {
      this.overlayRef.detach();
      this.overlayRef = null;
    }
  }

  addSetTagName() {}

  okSetTag() {}

  cancelSetTag() {}
}
