import { Component, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { TreeComponent } from 'angular-tree-component';

@Component({
  selector: 'mailbox-tree',
  templateUrl: './tree.html',
  styleUrls: ['./tree.scss'],
  preserveWhitespaces: false
})
export class MailboxTree<T> {

  @Input() isRemovable;
  @Input() nodes: T[];
  @Output() nodeSelect = new EventEmitter<T>();
  @ViewChild('tree') treeInstance: TreeComponent;

  removeBox() {
    console.log('call removeBox');
  }
}
