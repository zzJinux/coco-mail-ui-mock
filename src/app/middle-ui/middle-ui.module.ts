import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared.module';
import { TagSelector } from './tag-selector';
import { MovecopyPopup } from './movecopy-popup';
import { ContactsDialog } from './contacts-dialog';
import { RecentImportantDialog } from './recent-important-dialog';
import { NouserDialog } from './nouser-dialog';
import { MailboxTree } from './tree';

import { PopupTest } from './popup';

@NgModule({
    imports: [CommonModule, FormsModule, SharedModule],
    declarations: [TagSelector, MovecopyPopup, MailboxTree, PopupTest, ContactsDialog, RecentImportantDialog, NouserDialog],
    entryComponents: [ContactsDialog, RecentImportantDialog, NouserDialog],
    exports: [TagSelector, MovecopyPopup, MailboxTree, PopupTest, ContactsDialog, RecentImportantDialog, NouserDialog]
})
export class MiddleUIModule { }
