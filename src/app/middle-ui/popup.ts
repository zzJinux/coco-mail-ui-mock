import { Component, TemplateRef, ViewChild, OnInit, Input } from '@angular/core';
import { OverlayRef, OverlayConfig, Overlay, CdkOverlayOrigin } from '@angular/cdk/overlay';
import { CdkPortal } from '@angular/cdk/portal';

const EMAILS = [
  'vustthat@gmail.com',
  'sookwook2@naver.com',
  'tachkyon@gmail.com',
  'pipopipo@gmail.com'
];

@Component({
  selector: 'popup-test',
  exportAs: 'popupTest',
  templateUrl: './popup.html',
  styleUrls: ['./popup.scss'],
  preserveWhitespaces: false
})
export class PopupTest {

  @ViewChild('groupSelector') cdkPortal: CdkPortal;
  @Input() overlayOrigin: CdkOverlayOrigin;

  overlayRef: OverlayRef;

  reply_time = '17-11-07 01:27';
  forward_time = '17-11-07 01:27';
  reply_list = EMAILS;
  forward_list = EMAILS;

  /*  */
  reserved_date: Date;
  reserved = {
    timezone: "+0",
    hour: 0,
    option_hours: Array.from(Array(24)).map((_, idx) => idx),
    minute: 0,
    option_minutes: Array.from(Array(60)).map((_, idx) => idx)
  };
  /*  */

  /*  */
  nodeDatas = [
    { sn: 1, name: '구구구구구' },
    {
      sn: 2, name: '하하하하핳', children: [
        { sn: 21, name: '구구구구구' },
        { sn: 22, name: '구구구구구' }
      ]
    },
    { sn: 3, name: 'ㄴㅇㄹㄴㅇㄹ' }
  ];

  multiple = true;
  /*  */

  constructor(private overlay: Overlay) { }

  ngOnInit() {
    this.makePopup(this.overlayOrigin);
  }

  makePopup(origin: CdkOverlayOrigin) {
    const config = new OverlayConfig();
    config.positionStrategy = this.overlay.position().connectedTo(
      origin.elementRef,
      { originX: 'start', originY: 'top' },
      { overlayX: 'start', overlayY: 'top' });
    config.hasBackdrop = true;
    config.backdropClass = 'cdk-overlay-transparent-backdrop';

    this.overlayRef = this.overlay.create(config);
    this.overlayRef.backdropClick().subscribe(() => {
      // this.overlayRef.detach();
    });
    this.overlayRef.attach(this.cdkPortal);
    // this.overlayRef.backdropClick().subscribe(this.closePopup.bind(this));

    // timer(100).subscribe...
  }

  closePopup() {
    if (this.overlayRef) {
      this.overlayRef.detach();
      this.overlayRef = null;
    }
  }

  ngOnDestroy() {
    this.closePopup();
  }

  okReserved() { }

  cancelReserved() { }

  onGroupBlur() {
  }

  onGroupActivate() {
  }

  test(node: any, $event: boolean) {
    console.log(`${node.data.sn}: ${$event}`)
  }
}
