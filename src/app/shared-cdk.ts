import { NgModule } from '@angular/core';
import { CdkTableModule } from '@angular/cdk/table';
import { PortalModule } from '@angular/cdk/portal';
import { OverlayModule } from '@angular/cdk/overlay';

@NgModule({
  imports: [
    CdkTableModule,
    PortalModule,
    OverlayModule,
  ],
  exports: [
    CdkTableModule,
    PortalModule,
    OverlayModule
  ]
})
export class SharedCdkModule { }
