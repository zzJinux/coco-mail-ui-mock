import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { SharedModule } from './shared.module';
import { MiddleUIModule } from './middle-ui';
import { MailMenuModule } from './mail-menu/mail-menu.module';
import { MailListModule } from './mail-list/mail-list.module';
import { MailRWModule } from './mail-rw/mail-rw.module';
import { MailConfModule } from './mail-conf/mail-conf.module';
import { ContactsSelectorModule } from './contacts-selector/contacts-selector.module';

import { AppComponent } from './app.component';
import { RatingInputComponent } from './rating-input.component';

@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot([], { useHash: true }),
    FormsModule,
    SharedModule,
    MiddleUIModule,
    MailMenuModule,
    MailListModule,
    MailRWModule,
    MailConfModule,
    ContactsSelectorModule
  ],
  declarations: [AppComponent, RatingInputComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
