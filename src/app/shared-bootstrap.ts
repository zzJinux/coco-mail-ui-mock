import { NgModule } from '@angular/core';
import {
  BsDropdownModule,
  ModalModule,
  BsModalService,
  PaginationModule,
  BsDatepickerModule,
  TabsModule,
  AccordionModule,
  TooltipModule,
  ProgressbarModule
} from 'ngx-bootstrap';

@NgModule({
  imports: [
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TabsModule.forRoot(),
    AccordionModule.forRoot(),
    TooltipModule.forRoot(),
    ProgressbarModule.forRoot()
  ],
  exports: [
    BsDropdownModule,
    ModalModule,
    PaginationModule,
    BsDatepickerModule,
    TabsModule,
    AccordionModule,
    TooltipModule,
    ProgressbarModule
  ]
})
export class SharedBootstrapModule {
  constructor(modal: BsModalService) {
    Object.assign(modal.config, {
      animated: false,
      ignoreBackdropClick: true,
      keyboard: false,
      class: 'app-bs-modal'
    });
  }
}
