import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared.module';

import { DeptSelector } from './dept';

const ONLY_DECL = [
];
const EXPORTS = [DeptSelector];

@NgModule({
  imports: [CommonModule, RouterModule, SharedModule],
  declarations: [ONLY_DECL, EXPORTS],
  exports: EXPORTS
})
export class ContactsSelectorModule { }
