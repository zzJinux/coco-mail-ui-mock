import { NgModule } from '@angular/core';

import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

/**
 * 주어진 time, format으로 moment library를 이용하여 날짜문자열을 구한다.
 * value: 시간(milliseconds)
 * format: 날짜변환형식(예:YY-MM-DD HH:mm)
 */
@Pipe({ name: 'datex' })
export class DatexPipe implements PipeTransform {
  transform(value: number | Date, format: string = ''): string {
    if (!value) { return ''; }
    return moment(value).format(format);
  }
}

/**
 * /byte단위의 정수갑(파일사이즈등)을   B, K, M, G로 변환한다.(234B, 23.2K 145.9M, 333.1G등)
 * size: 바이트 단위의 크기
 */
@Pipe({ name: 'filesize' })
export class FilesizePipe implements PipeTransform {
  transform(size: number): string {
    let str = '';
    if (size < 1024) {
      str = size + 'B';
    } else if (size < 1048576) {
      str = (Math.round((size / 1024) * 10) / 10) + 'K';
    } else if (size < 1073741824) {
      str = (Math.round((size / 1048576) * 10) / 10) + 'M';
    } else {
      str = (Math.round((size / 1073741824) * 10) / 10) + 'G';
    }
    return str;
  }
}

/**
 * /byte단위의 정수갑(파일사이즈등)을   Byte, KB, MB, GB로 변환한다.(234Byte, 23.2KB 145.9MB, 333.1GB등)
 * size: 바이트 단위의 크기
 */
@Pipe({ name: 'unitSize' })
export class UnitSizePipe implements PipeTransform {
  transform(size: number): string {
    let str = `${size}Byte`;
    if (size > 1073741823) {
      str = (Math.round((size / 1073741824) * 10) / 10) + 'GB';
    }
    else if (size > 1048575) {
      str = (Math.round((size / 1048576) * 10) / 10) + 'MB';
    }
    else if (size > 1023) {
      str = (Math.round((size / 1024) * 10) / 10) + 'KB';
    }

    return str;
  }
}

@Pipe({ name: 'priority_class' })
export class PriorityClassPipe implements PipeTransform {
  transform(flag: number): string {
    let str = 'fa-';
    if (flag === 1) {
      str += 'star';
    } else if (flag === 2) {
      str += 'star-half-o';
    } else {
      str += 'star-o';
    }
    return str;
  }
}

@Pipe({ name: 'attach_class' })
export class AttachClassPipe implements PipeTransform {
  transform(flag: number): string {
    let str = '';
    if (flag === 1) {
      str = 'fa-paperclip';
    } else if (flag === 2) {
      str = 'fa-link';
    } else {
      str = '';
    }
    return str;
  }
}

@Pipe({ name: 'read_class' })
export class ReadClassPipe implements PipeTransform {
  transform(bean: any): string {
    // read_flag, lock_flag
    let str = 'fa-';
    if (bean.lock_flag === 1) {
      str += 'lock';
    } else if (bean.read_flag === 1) {
      str += 'envelope';
    } else {
      str += 'envelope-o';
    }
    return str;
  }
}

@Pipe({ name: 'checkall_class' })
export class CheckallClassPipe implements PipeTransform {
  transform(checkall: boolean): string {
    if (checkall) return 'fa-check-square-o';
    else return 'fa-square-o';
  }
}

@Pipe({ name: 'from_name_email' })
export class FromNameEmailPipe implements PipeTransform {
  transform(bean: any): string {
    let from: string = '<' + bean.from_email + '>';
    if (bean.from_name != null) from = '"' + bean.from_name + '" ' + from;
    return from;
  }
}

@Pipe({ name: 'recipient' })
export class RecipientPipe implements PipeTransform {
  transform(email: string, name: string): string {
    let str = '';
    if (name) str = '"' + name + '" <' + email + '>';
    else str = email;

    return str;
  }
}


@Pipe({ name: 'r_mailbox_list' })
export class RMailboxListPipe implements PipeTransform {
  transform(data: number[], mailbox_o: any): number[] {
    if (Array.isArray(data)) {
      // let list: string[]=[];

      // data.forEach((sn,idx,arr)=>{
      // 	if(sn!='9100' && mailbox_o[sn].type==0) list.push(sn);
      // });

      // return list;

      return data.filter((sn, idx, arr) => {
        return (sn !== 9100 && mailbox_o[sn].type === 0);
      });
    }
    else {
      return data;
    }
  }
}

@Pipe({ name: 'upper_mailbox_list' })
export class UpperMailboxListPipe implements PipeTransform {
  transform(data: string[], mailbox_o: any): string[] {
    if (Array.isArray(data)) {
      // let list: string[]=[];

      // data.forEach((sn:string,idx:number,arr:string[])=>{

      // 	console.log('UpperMailboxListPipe sn:'+sn);

      // 	if(mailbox_o[sn].levels>1) list.push(sn);
      // });

      // return list;

      return data.filter((sn, idx, arr) => {
        return mailbox_o[sn].levels > 1;
      });
    }
    else {
      return data;
    }
  }
}

@Pipe({ name: 'find_upper_node' })
export class FindUpperNodePipe implements PipeTransform {
  transform(upper_nodes: any[], upper_path: string, s_levels: number): any {
    if (Array.isArray(upper_nodes)) {
      let upper_node: any = null;
      const path_list: string[] = upper_path.substring(1, upper_path.length - 1).split('/');

      for (let i: number = (s_levels - 1); i < path_list.length; i++) {
        const sn: string = path_list[i];
        // console.log('FindUpperNodePipe upper_list sn: '+sn);

        for (let j = 0; j < upper_nodes.length; j++) {
          // console.log('FindUpperNodePipe upper_nodes[j].sn: '+upper_nodes[j].sn);
          if (upper_nodes[j].sn === sn) {
            upper_node = upper_nodes[j];
            // console.log('FindUpperNodePipe upper_node find: ');
            break;
          }
          // console.log('FindUpperNodePipe upper_nodes sn '+sn);
        }

        if (upper_node) upper_nodes = upper_node.children ? upper_node.children : undefined;
      }

      return upper_node;
    }
    else {
      return undefined;
    }
  }
}

@NgModule({
  declarations: [
    DatexPipe,
    FilesizePipe,
    UnitSizePipe,
    PriorityClassPipe,
    AttachClassPipe,
    ReadClassPipe,
    CheckallClassPipe,
    FromNameEmailPipe,
    RecipientPipe,
    RMailboxListPipe,
    UpperMailboxListPipe,
    FindUpperNodePipe],
  exports: [
    DatexPipe,
    FilesizePipe,
    UnitSizePipe,
    PriorityClassPipe,
    AttachClassPipe,
    ReadClassPipe,
    CheckallClassPipe,
    FromNameEmailPipe,
    RecipientPipe,
    RMailboxListPipe,
    UpperMailboxListPipe,
    FindUpperNodePipe]
})
export class CocoPipeModule {

}
