export interface ReadStateInfo {
  name?: string;
  email: string;
  state_code: number;
  send_time?: Date;
  reserve_time?: Date;
  local?: number;
  readok_time?: Date;
  cancel_time?: Date;
}

export interface ReakokItem {
  sn?: number;
  subject?: string;
  list: ReadStateInfo[];
  readok: number;
  show?: boolean;
}

export const TEST_SET1: ReakokItem[] = [
  {
    'list': [
      {
        'state_code': -2,
        'email': 'sookwook2@naver.com',
        'name': null
      }
    ],
    'readok': 55,
    'subject': null
  },
  {
    'list': [
      {
        'state_code': -1,
        'email': 'sookwook2@naver.com',
        'name': null
      }
    ],
    'readok': 55,
    'subject': null
  },
  {
    'list': [
      {
        'state_code': 0,
        'email': 'sookwook2@naver.com',
        'name': null
      }
    ],
    'readok': 55,
    'subject': null
  },
  {
    'list': [
      {
        'state_code': 1,
        'email': 'sookwook2@naver.com',
        'name': null
      }
    ],
    'readok': 55,
    'subject': null
  },
  {
    'list': [
      {
        'state_code': -2,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      }
    ],
    'readok': 55,
    'subject': null
  },
  {
    'list': [
      {
        'state_code': -1,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      }
    ],
    'readok': 55,
    'subject': null
  },
  {
    'list': [
      {
        'state_code': 0,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      }
    ],
    'readok': 55,
    'subject': null
  },
  {
    'list': [
      {
        'state_code': 1,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      }
    ],
    'readok': 55,
    'subject': null
  },
  {
    'list': [
      {
        'state_code': -2,
        'email': 'sookwook2@naver.com',
        'name': null
      }
    ],
    'readok': 55,
    'subject': '님은 갔습니다. 아아, 사랑하는 나의 님은 갔습니다.'
  },
  {
    'list': [
      {
        'state_code': -1,
        'email': 'sookwook2@naver.com',
        'name': null
      }
    ],
    'readok': 55,
    'subject': '님은 갔습니다. 아아, 사랑하는 나의 님은 갔습니다.'
  },
  {
    'list': [
      {
        'state_code': 0,
        'email': 'sookwook2@naver.com',
        'name': null
      }
    ],
    'readok': 55,
    'subject': '님은 갔습니다. 아아, 사랑하는 나의 님은 갔습니다.'
  },
  {
    'list': [
      {
        'state_code': 1,
        'email': 'sookwook2@naver.com',
        'name': null
      }
    ],
    'readok': 55,
    'subject': '님은 갔습니다. 아아, 사랑하는 나의 님은 갔습니다.'
  },
  {
    'list': [
      {
        'state_code': -2,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      }
    ],
    'readok': 55,
    'subject': '님은 갔습니다. 아아, 사랑하는 나의 님은 갔습니다.'
  },
  {
    'list': [
      {
        'state_code': -1,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      }
    ],
    'readok': 55,
    'subject': '님은 갔습니다. 아아, 사랑하는 나의 님은 갔습니다.'
  },
  {
    'list': [
      {
        'state_code': 0,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      }
    ],
    'readok': 55,
    'subject': '님은 갔습니다. 아아, 사랑하는 나의 님은 갔습니다.'
  },
  {
    'list': [
      {
        'state_code': 1,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      }
    ],
    'readok': 55,
    'subject': '님은 갔습니다. 아아, 사랑하는 나의 님은 갔습니다.'
  },
  {
    'list': [
      {
        'state_code': -2,
        'email': 'sookwook2@naver.com',
        'name': null
      }
    ],
    'readok': 55,
    'subject': '사랑도 사람의 일이라 만날 때에 미리 떠날 것을 염려하고 경계하지 아니한 것은 아니지만, 이별은 뜻밖의 일이 되고 놀란 가슴은 새로운 슬픔에 터집니다.'
  },
  {
    'list': [
      {
        'state_code': -1,
        'email': 'sookwook2@naver.com',
        'name': null
      }
    ],
    'readok': 55,
    'subject': '사랑도 사람의 일이라 만날 때에 미리 떠날 것을 염려하고 경계하지 아니한 것은 아니지만, 이별은 뜻밖의 일이 되고 놀란 가슴은 새로운 슬픔에 터집니다.'
  },
  {
    'list': [
      {
        'state_code': 0,
        'email': 'sookwook2@naver.com',
        'name': null
      }
    ],
    'readok': 55,
    'subject': '사랑도 사람의 일이라 만날 때에 미리 떠날 것을 염려하고 경계하지 아니한 것은 아니지만, 이별은 뜻밖의 일이 되고 놀란 가슴은 새로운 슬픔에 터집니다.'
  },
  {
    'list': [
      {
        'state_code': 1,
        'email': 'sookwook2@naver.com',
        'name': null
      }
    ],
    'readok': 55,
    'subject': '사랑도 사람의 일이라 만날 때에 미리 떠날 것을 염려하고 경계하지 아니한 것은 아니지만, 이별은 뜻밖의 일이 되고 놀란 가슴은 새로운 슬픔에 터집니다.'
  },
  {
    'list': [
      {
        'state_code': -2,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      }
    ],
    'readok': 55,
    'subject': '사랑도 사람의 일이라 만날 때에 미리 떠날 것을 염려하고 경계하지 아니한 것은 아니지만, 이별은 뜻밖의 일이 되고 놀란 가슴은 새로운 슬픔에 터집니다.'
  },
  {
    'list': [
      {
        'state_code': -1,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      }
    ],
    'readok': 55,
    'subject': '사랑도 사람의 일이라 만날 때에 미리 떠날 것을 염려하고 경계하지 아니한 것은 아니지만, 이별은 뜻밖의 일이 되고 놀란 가슴은 새로운 슬픔에 터집니다.'
  },
  {
    'list': [
      {
        'state_code': 0,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      }
    ],
    'readok': 55,
    'subject': '사랑도 사람의 일이라 만날 때에 미리 떠날 것을 염려하고 경계하지 아니한 것은 아니지만, 이별은 뜻밖의 일이 되고 놀란 가슴은 새로운 슬픔에 터집니다.'
  },
  {
    'list': [
      {
        'state_code': 1,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      }
    ],
    'readok': 55,
    'subject': '사랑도 사람의 일이라 만날 때에 미리 떠날 것을 염려하고 경계하지 아니한 것은 아니지만, 이별은 뜻밖의 일이 되고 놀란 가슴은 새로운 슬픔에 터집니다.'
  }
];

export const TEST_SET2: ReakokItem[] = [
  {
    'list': [
      {
        'state_code': -2,
        'email': 'sookwook2@naver.com',
        'name': null
      },
      {
        'state_code': -1,
        'email': 'sookwook2@naver.com',
        'name': null
      },
      {
        'state_code': 0,
        'email': 'sookwook2@naver.com',
        'name': null
      },
      {
        'state_code': 1,
        'email': 'sookwook2@naver.com',
        'name': null
      }
    ],
    'readok': 55,
    'subject': null
  },
  {
    'list': [
      {
        'state_code': 0,
        'email': 'sookwook2@naver.com',
        'name': null
      },
      {
        'state_code': 1,
        'email': 'sookwook2@naver.com',
        'name': null
      },
      {
        'state_code': -2,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      },
      {
        'state_code': -1,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      }
    ],
    'readok': 55,
    'subject': null
  },
  {
    'list': [
      {
        'state_code': -2,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      },
      {
        'state_code': -1,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      },
      {
        'state_code': 0,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      },
      {
        'state_code': 1,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      }
    ],
    'readok': 55,
    'subject': null
  },
  {
    'list': [
      {
        'state_code': -2,
        'email': 'sookwook2@naver.com',
        'name': null
      },
      {
        'state_code': -1,
        'email': 'sookwook2@naver.com',
        'name': null
      },
      {
        'state_code': 0,
        'email': 'sookwook2@naver.com',
        'name': null
      },
      {
        'state_code': 1,
        'email': 'sookwook2@naver.com',
        'name': null
      }
    ],
    'readok': 55,
    'subject': '님은 갔습니다. 아아, 사랑하는 나의 님은 갔습니다.'
  },
  {
    'list': [
      {
        'state_code': 0,
        'email': 'sookwook2@naver.com',
        'name': null
      },
      {
        'state_code': 1,
        'email': 'sookwook2@naver.com',
        'name': null
      },
      {
        'state_code': -2,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      },
      {
        'state_code': -1,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      }
    ],
    'readok': 55,
    'subject': '님은 갔습니다. 아아, 사랑하는 나의 님은 갔습니다.'
  },
  {
    'list': [
      {
        'state_code': -2,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      },
      {
        'state_code': -1,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      },
      {
        'state_code': 0,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      },
      {
        'state_code': 1,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      }
    ],
    'readok': 55,
    'subject': '님은 갔습니다. 아아, 사랑하는 나의 님은 갔습니다.'
  },
  {
    'list': [
      {
        'state_code': -2,
        'email': 'sookwook2@naver.com',
        'name': null
      },
      {
        'state_code': -1,
        'email': 'sookwook2@naver.com',
        'name': null
      },
      {
        'state_code': 0,
        'email': 'sookwook2@naver.com',
        'name': null
      },
      {
        'state_code': 1,
        'email': 'sookwook2@naver.com',
        'name': null
      }
    ],
    'readok': 55,
    'subject': '사랑도 사람의 일이라 만날 때에 미리 떠날 것을 염려하고 경계하지 아니한 것은 아니지만, 이별은 뜻밖의 일이 되고 놀란 가슴은 새로운 슬픔에 터집니다.'
  },
  {
    'list': [
      {
        'state_code': 0,
        'email': 'sookwook2@naver.com',
        'name': null
      },
      {
        'state_code': 1,
        'email': 'sookwook2@naver.com',
        'name': null
      },
      {
        'state_code': -2,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      },
      {
        'state_code': -1,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      }
    ],
    'readok': 55,
    'subject': '사랑도 사람의 일이라 만날 때에 미리 떠날 것을 염려하고 경계하지 아니한 것은 아니지만, 이별은 뜻밖의 일이 되고 놀란 가슴은 새로운 슬픔에 터집니다.'
  },
  {
    'list': [
      {
        'state_code': -2,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      },
      {
        'state_code': -1,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      },
      {
        'state_code': 0,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      },
      {
        'state_code': 1,
        'email': 'sookwook2@naver.com',
        'name': '물병옥'
      }
    ],
    'readok': 55,
    'subject': '사랑도 사람의 일이라 만날 때에 미리 떠날 것을 염려하고 경계하지 아니한 것은 아니지만, 이별은 뜻밖의 일이 되고 놀란 가슴은 새로운 슬픔에 터집니다.'
  }
];
