(function() {
  class IG {
    constructor(array) { this.array = array; }
    chainPopulate(property, values) {
      return new IG(
        this.array.reduce(
          (acc, cur) => acc.concat(
            values.map(val => Object.assign({ [property]: val }, cur))
          ), []
        )
      );
    }
  }

  const SUBJECTS =[
    '님은 갔습니다. 아아, 사랑하는 나의 님은 갔습니다.',
    '사랑도 사람의 일이라 만날 때에 미리 떠날 것을 염려하고 경계하지 아니한 것은 아니지만, 이별은 뜻밖의 일이 되고 놀란 가슴은 새로운 슬픔에 터집니다.'
  ];

  const stateInfo = [
    ['name', [null, '물병옥']],
    ['email', ['sookwook2@naver.com']],
    ['state_code', [-2, -1, 0, 1]]
  ];

  const states = stateInfo.reduce((prev, cur) => prev.chainPopulate(...cur), new IG([{}])).array;

  const readokItemType1 = [
    ['subject', [null, ...SUBJECTS]],
    ['readok', [55]],
    ['list', [...states.map(val => [val])]]
  ];

  const readokItemType2 = [
    ['subject', [null, ...SUBJECTS]],
    ['readok', [55]],
    ['list', [states.slice(0, 4), states.slice(2, 6), states.slice(4, 8)]]
  ];

  return readokItemType1.reduce((prev, cur) => prev.chainPopulate(...cur), new IG([{}])).array;
})();
