export const NODES_TEST = [
  {
    id: 1,
    name: '개인메일함1',
    notread: 0,
    remove: true,
    children: [
      {
        id: 2,
        name: '개인메일함2',
        notread: 3,
        remove: true,
        children: [
          {
            id: 5,
            name: '개인메일함5',
            notread: 0,
            remove: true,
          }
        ]
      },
      {
        id: 3,
        name: '개인메일함3',
        notread: 0,
        remove: true
      }
    ]
  },
  {
    id: 4,
    name: '개인메일함4',
    notread: 0,
    remove: true
  }
];
