export interface MailItem {
  checked?: boolean;
  hidden?: boolean;
  from_email?: string;
  from_name?: string;
  sn: number;
  read_flag: 0 | 1;
  file_flag: 0 | 1 | 2 | 3;
  lock_flag: 0 | 1;
  subject: string;
  reply_flag: 0 | 1 | 2;
  forward_flag: 0 | 1;
  priority_flag: 0 | 1 | 2 | 3;
}

export const MAILS_TEST: MailItem[] = [
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 0,
    'read_flag': 0,
    'subject': '날마다 바다에서 수평선이 가라앉아 죽는다.',
    'sn': 1
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 0,
    'read_flag': 0,
    'subject': '하나의 긍정과 부정 사이에서',
    'sn': 2
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 0,
    'read_flag': 0,
    'subject': '누군가가 하나의 직선으로 죽어서 ',
    'sn': 3
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 0,
    'read_flag': 0,
    'subject': '아무도 몰래 바다를 그 너머로 이어준다.',
    'sn': 4
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 0,
    'read_flag': 0,
    'subject': '다시 울음처럼 소생하기를 바라지만',
    'sn': 5
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 0,
    'read_flag': 0,
    'subject': '저 스스로 바다는 넓게 넓게 밝혀서',
    'sn': 6
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 0,
    'read_flag': 0,
    'subject': '어떤 그림자도 바다에는 떨어질 수 없다.',
    'sn': 7
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 0,
    'read_flag': 0,
    'subject': '보라, 저 수평선에서 누가 지옥처럼 살고 있는가.',
    'sn': 8
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 0,
    'read_flag': 0,
    'subject': '저 수평선에서 누가 소나기처럼 울고 있는가.',
    'sn': 9
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 0,
    'read_flag': 0,
    'subject': '바다 위의 일망무제여 바다여 바다 실존이여',
    'sn': 10
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 0,
    'read_flag': 0,
    'subject': '바라볼수록 떠오르는 위경련의 고독',
    'sn': 11
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 0,
    'read_flag': 0,
    'subject': '모든 곳에서 모인 고독이 비로소 아픔 너머 쉬고 있다.',
    'sn': 12
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 0,
    'read_flag': 0,
    'subject': '그러나 바다 위에서 고독은 또 기다린다.',
    'sn': 13
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 0,
    'read_flag': 0,
    'subject': '하나의 첫날밤을 만들기 위하여',
    'sn': 14
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 0,
    'read_flag': 0,
    'subject': '하나의 공화국을 건설허기 위하여',
    'sn': 15
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 0,
    'read_flag': 0,
    'subject': '고독은 바다 전체에 자유를 알알이 깐다.',
    'sn': 16
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 1,
    'read_flag': 0,
    'subject': '드디어 나는 큰 소리로 자유를 불렀다.',
    'sn': 17
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 1,
    'read_flag': 0,
    'subject': '누구처럼 어느 곳처럼 자유를 불렀다.',
    'sn': 18
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 1,
    'read_flag': 0,
    'subject': '부르자마자 바다는 일제히 파도치기 시작한다.',
    'sn': 19
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 1,
    'read_flag': 0,
    'subject': '이제까지 기다렸던 바다 속의 어둠과',
    'sn': 20
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 1,
    'read_flag': 0,
    'subject': '바다 위의 고독과 자유 모든 절망으로부터 ',
    'sn': 21
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 1,
    'read_flag': 0,
    'subject': '바다는 큰 소리로 파도치기 시작한다.',
    'sn': 22
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 1,
    'read_flag': 0,
    'subject': '죽음은 도저히 죽음만으로 엄숙하지 않고',
    'sn': 23
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 1,
    'read_flag': 0,
    'subject': '죽음 속에서 삶이 먼동처럼 터지며 ',
    'sn': 24
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 1,
    'read_flag': 0,
    'subject': '바다 전체의 파도마다 죽음을 나누어 준다.',
    'sn': 25
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 1,
    'read_flag': 0,
    'subject': '이미 나의 부르는 소리는 천벌로 막혀버리고',
    'sn': 26
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 1,
    'read_flag': 0,
    'subject': '바다 위로 긋는 마른 번개여 첫날밤이여',
    'sn': 27
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 1,
    'read_flag': 0,
    'subject': '모든 바닷가에 일망무제의 고독이 감태처럼 밀린다.',
    'sn': 28
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 1,
    'read_flag': 0,
    'subject': '보라 저 파도 위에 하나의 배가 떠오른다.',
    'sn': 29
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 1,
    'read_flag': 0,
    'subject': '돛이 찢어지고 돛기둥이 잘린 채 ',
    'sn': 30
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 1,
    'read_flag': 0,
    'subject': '언젠가 만나리라는 뜻만이 용골처럼 남아서 ',
    'sn': 31
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 1,
    'read_flag': 0,
    'subject': '파도마다 그것을 삼키려 하지만 ',
    'sn': 32
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 3,
    'read_flag': 0,
    'subject': '꼭 만나리라는 뜻은 삼킬 수 있겠는가.',
    'sn': 33
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 3,
    'read_flag': 0,
    'subject': '죽음 속에서 삶은 먼동 트여서',
    'sn': 34
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 3,
    'read_flag': 0,
    'subject': '파도 이랑에 숨었다가 떠오른 배를 만난다.',
    'sn': 35
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 3,
    'read_flag': 0,
    'subject': '아아 배 위의 삶이 일어서서 손을 흔든다.',
    'sn': 36
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 3,
    'read_flag': 0,
    'subject': '찢어진 돛으로 파도 속에서 흔들리면서',
    'sn': 37
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 3,
    'read_flag': 0,
    'subject': '그토록 기다린 삶으로 배는 나간다.',
    'sn': 38
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 3,
    'read_flag': 0,
    'subject': '모든 파도는 배가 가는 앞뒤에서 어둠에 덮인다.',
    'sn': 39
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 3,
    'read_flag': 0,
    'subject': '그리하여 내일이 오면 만남은 또 무엇이 되는가',
    'sn': 40
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 3,
    'read_flag': 0,
    'subject': '날마다 바다에서 수평선이 가라앉아 죽는다.',
    'sn': 41
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 3,
    'read_flag': 0,
    'subject': '하나의 긍정과 부정 사이에서',
    'sn': 42
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 3,
    'read_flag': 0,
    'subject': '누군가가 하나의 직선으로 죽어서 ',
    'sn': 43
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 3,
    'read_flag': 0,
    'subject': '아무도 몰래 바다를 그 너머로 이어준다.',
    'sn': 44
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 3,
    'read_flag': 0,
    'subject': '다시 울음처럼 소생하기를 바라지만',
    'sn': 45
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 3,
    'read_flag': 0,
    'subject': '저 스스로 바다는 넓게 넓게 밝혀서',
    'sn': 46
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 3,
    'read_flag': 0,
    'subject': '어떤 그림자도 바다에는 떨어질 수 없다.',
    'sn': 47
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 3,
    'read_flag': 0,
    'subject': '보라, 저 수평선에서 누가 지옥처럼 살고 있는가.',
    'sn': 48
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 0,
    'read_flag': 1,
    'subject': '저 수평선에서 누가 소나기처럼 울고 있는가.',
    'sn': 49
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 0,
    'read_flag': 1,
    'subject': '바다 위의 일망무제여 바다여 바다 실존이여',
    'sn': 50
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 0,
    'read_flag': 1,
    'subject': '바라볼수록 떠오르는 위경련의 고독',
    'sn': 51
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 0,
    'read_flag': 1,
    'subject': '모든 곳에서 모인 고독이 비로소 아픔 너머 쉬고 있다.',
    'sn': 52
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 0,
    'read_flag': 1,
    'subject': '그러나 바다 위에서 고독은 또 기다린다.',
    'sn': 53
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 0,
    'read_flag': 1,
    'subject': '하나의 첫날밤을 만들기 위하여',
    'sn': 54
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 0,
    'read_flag': 1,
    'subject': '하나의 공화국을 건설허기 위하여',
    'sn': 55
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 0,
    'read_flag': 1,
    'subject': '고독은 바다 전체에 자유를 알알이 깐다.',
    'sn': 56
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 0,
    'read_flag': 1,
    'subject': '드디어 나는 큰 소리로 자유를 불렀다.',
    'sn': 57
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 0,
    'read_flag': 1,
    'subject': '누구처럼 어느 곳처럼 자유를 불렀다.',
    'sn': 58
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 0,
    'read_flag': 1,
    'subject': '부르자마자 바다는 일제히 파도치기 시작한다.',
    'sn': 59
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 0,
    'read_flag': 1,
    'subject': '이제까지 기다렸던 바다 속의 어둠과',
    'sn': 60
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 0,
    'read_flag': 1,
    'subject': '바다 위의 고독과 자유 모든 절망으로부터 ',
    'sn': 61
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 0,
    'read_flag': 1,
    'subject': '바다는 큰 소리로 파도치기 시작한다.',
    'sn': 62
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 0,
    'read_flag': 1,
    'subject': '죽음은 도저히 죽음만으로 엄숙하지 않고',
    'sn': 63
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 0,
    'read_flag': 1,
    'subject': '죽음 속에서 삶이 먼동처럼 터지며 ',
    'sn': 64
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 1,
    'read_flag': 1,
    'subject': '바다 전체의 파도마다 죽음을 나누어 준다.',
    'sn': 65
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 1,
    'read_flag': 1,
    'subject': '이미 나의 부르는 소리는 천벌로 막혀버리고',
    'sn': 66
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 1,
    'read_flag': 1,
    'subject': '바다 위로 긋는 마른 번개여 첫날밤이여',
    'sn': 67
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 1,
    'read_flag': 1,
    'subject': '모든 바닷가에 일망무제의 고독이 감태처럼 밀린다.',
    'sn': 68
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 1,
    'read_flag': 1,
    'subject': '보라 저 파도 위에 하나의 배가 떠오른다.',
    'sn': 69
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 1,
    'read_flag': 1,
    'subject': '돛이 찢어지고 돛기둥이 잘린 채 ',
    'sn': 70
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 1,
    'read_flag': 1,
    'subject': '언젠가 만나리라는 뜻만이 용골처럼 남아서 ',
    'sn': 71
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 1,
    'read_flag': 1,
    'subject': '파도마다 그것을 삼키려 하지만 ',
    'sn': 72
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 1,
    'read_flag': 1,
    'subject': '꼭 만나리라는 뜻은 삼킬 수 있겠는가.',
    'sn': 73
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 1,
    'read_flag': 1,
    'subject': '죽음 속에서 삶은 먼동 트여서',
    'sn': 74
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 1,
    'read_flag': 1,
    'subject': '파도 이랑에 숨었다가 떠오른 배를 만난다.',
    'sn': 75
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 1,
    'read_flag': 1,
    'subject': '아아 배 위의 삶이 일어서서 손을 흔든다.',
    'sn': 76
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 1,
    'read_flag': 1,
    'subject': '찢어진 돛으로 파도 속에서 흔들리면서',
    'sn': 77
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 1,
    'read_flag': 1,
    'subject': '그토록 기다린 삶으로 배는 나간다.',
    'sn': 78
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 1,
    'read_flag': 1,
    'subject': '모든 파도는 배가 가는 앞뒤에서 어둠에 덮인다.',
    'sn': 79
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 1,
    'read_flag': 1,
    'subject': '그리하여 내일이 오면 만남은 또 무엇이 되는가',
    'sn': 80
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 3,
    'read_flag': 1,
    'subject': '날마다 바다에서 수평선이 가라앉아 죽는다.',
    'sn': 81
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 3,
    'read_flag': 1,
    'subject': '하나의 긍정과 부정 사이에서',
    'sn': 82
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 3,
    'read_flag': 1,
    'subject': '누군가가 하나의 직선으로 죽어서 ',
    'sn': 83
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 0,
    'file_flag': 3,
    'read_flag': 1,
    'subject': '아무도 몰래 바다를 그 너머로 이어준다.',
    'sn': 84
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 3,
    'read_flag': 1,
    'subject': '다시 울음처럼 소생하기를 바라지만',
    'sn': 85
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 3,
    'read_flag': 1,
    'subject': '저 스스로 바다는 넓게 넓게 밝혀서',
    'sn': 86
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 3,
    'read_flag': 1,
    'subject': '어떤 그림자도 바다에는 떨어질 수 없다.',
    'sn': 87
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 0,
    'file_flag': 3,
    'read_flag': 1,
    'subject': '보라, 저 수평선에서 누가 지옥처럼 살고 있는가.',
    'sn': 88
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 3,
    'read_flag': 1,
    'subject': '저 수평선에서 누가 소나기처럼 울고 있는가.',
    'sn': 89
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 3,
    'read_flag': 1,
    'subject': '바다 위의 일망무제여 바다여 바다 실존이여',
    'sn': 90
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 3,
    'read_flag': 1,
    'subject': '바라볼수록 떠오르는 위경련의 고독',
    'sn': 91
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 0,
    'lock_flag': 1,
    'file_flag': 3,
    'read_flag': 1,
    'subject': '모든 곳에서 모인 고독이 비로소 아픔 너머 쉬고 있다.',
    'sn': 92
  },
  {
    'priority_flag': 3,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 3,
    'read_flag': 1,
    'subject': '그러나 바다 위에서 고독은 또 기다린다.',
    'sn': 93
  },
  {
    'priority_flag': 1,
    'forward_flag': 0,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 3,
    'read_flag': 1,
    'subject': '하나의 첫날밤을 만들기 위하여',
    'sn': 94
  },
  {
    'priority_flag': 3,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 3,
    'read_flag': 1,
    'subject': '하나의 공화국을 건설허기 위하여',
    'sn': 95
  },
  {
    'priority_flag': 1,
    'forward_flag': 1,
    'reply_flag': 1,
    'lock_flag': 1,
    'file_flag': 3,
    'read_flag': 1,
    'subject': '고독은 바다 전체에 자유를 알알이 깐다.',
    'sn': 96
  }
];
