import { Component, Input, ViewChild, OnInit, ElementRef } from '@angular/core';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mail-read',
  templateUrl: './read.html',
  styleUrls: ['./read.scss'],
  preserveWhitespaces: false
})
// tslint:disable-next-line:component-class-suffix
export class MailRead implements OnInit {
  // tslint:disable-next-line:no-input-rename
  @Input('isNormalView') isListNormalView = true;
  // @ViewChild('topHeader') topHeader: ElementRef;
  // get headerPad() {
  //   return this.topHeader ? (<HTMLElement>this.topHeader.nativeElement).offsetHeight : 0;
  // }

  _css = { isPopupWindow: false };
  mailbox_name = '아무메일함';
  notread_cnt = 5;
  total_cnt = 10;
  subject = '호로롤홀홀호호로홀호';
  send_time = new Date;
  mail_size = 324234234;

  mail: any = {
    priority_flag: 1
  };

  from_name = '광광광';
  from_email = 'kwangkwangkwang@sdfsdf.com';

  isOpenToccbcc = false;
  openToccbccIcon = 'arrow_drop_down';

  to_view_mode = 0;
  cc_view_mode = 0;
  bcc_view_mode = 0;
  to_list = [
    { email: 'sdfsdf@asdff.com', name: '구구구' },
    { email: 'sdfsdf@asdff.com', name: '피나야' },
    { email: 'sdfsdf@asdff.com', name: '두구룩' }
  ];
  cc_list = this.to_list;
  bcc_list = this.to_list;

  isOpenAttachfile;
  openAttachfileIcon = 'arrow_drop_down';
  isOpenLargeAttachfile;
  openLargeAttachfileIcon = 'arrow_drop_down';

  filename_list = [
    '부엉이', '소쩍새', '돌고래', '범고래', '빡빡이', '구와아악'
  ];


  @ViewChild('contentFrame') contentFrame: ElementRef;
  // tslint:disable-next-line:max-line-length
  fake = `<head></head><body><style>*{margin: 0px;}</style><div style="font-size:9pt;color:#000000;font-family: 굴림, 굴림체, 돋움, 돋움체, 바탕, 바탕체, Arial, Verdana, sans-serif;"><p>cvcvx</p><br><br><div><table bgcolor="#ffffff" border="2" bordercolor="#aaa9a9" bordercolordark="#aaa9a9" bordercolorlight="#aaa9a9" cellpadding="0" cellspacing="0" style="border-collapse:collapse; border-image:none; color:#000000; font-family:굴림; font-size:14px; width:750px"><tbody><tr><td height="60" style="font-family: 맑은 고딕;"><p>&nbsp;<strong>(주)인컴&nbsp;</strong>&nbsp;이사&nbsp;&nbsp;/&nbsp;정승모<br>&nbsp;Email :&nbsp;jseungmo<a href="mailto:nsjeon@zettamail.com">@incom.land</a><br><span style="color:#ffa500"><strong>&nbsp;휴대폰</strong></span> : 010-5118-9496</p></td></tr></tbody></table></div></div><div align="right"><img src="http://mail.incom.land:4040/cocomail/mail/read/check_reading.nvd?mail_sn=279005&amp;r_email=jinwook@jw.incom.land" border="0"></div></body>`;

  filesize_total = 3058345;
  filesize_list = [5, 234, 23089, 2323, -1, 324];

  ngOnInit() {
    (<HTMLIFrameElement>this.contentFrame.nativeElement).contentDocument.write(this.fake);
  }

  openToccbcc() {
    this.isOpenToccbcc = !this.isOpenToccbcc;
    this.openToccbccIcon = this.isOpenToccbcc ? 'arrow_drop_up' : 'arrow_drop_down';
  }

  openAttachfile() {
    this.isOpenAttachfile = !this.isOpenAttachfile;
    this.openAttachfileIcon = this.isOpenAttachfile ? 'arrow_drop_up' : 'arrow_drop_down';
  }

  openLargeAttachfile() {
    this.isOpenLargeAttachfile = !this.isOpenLargeAttachfile;
    this.openLargeAttachfileIcon = this.isOpenLargeAttachfile ? 'arrow_drop_up' : 'arrow_drop_down';
  }

  setPriorityMail() { }
  saveFileAll() { }
  deleteFileAll() { }
  saveFile() { }
  deleteFile() { }
}
