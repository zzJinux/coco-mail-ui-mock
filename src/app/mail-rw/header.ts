import { Component, ViewChild } from '@angular/core';

@Component({
  selector: 'mail-read__header',
  templateUrl: './header.html',
  styleUrls: ['./header.css'],
  preserveWhitespaces: false
})
export class MailRead_Header {
  _css = { isPopupWindow: false };
}
