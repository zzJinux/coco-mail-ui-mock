import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared.module';
import { MiddleUIModule } from '../middle-ui';

import { MailRead_Header } from './header';
import { MailRead } from './read';
import { MailWrite } from './write';
import {MailWriteResult} from './write-result';

const DECLARATIONS = [MailRead_Header, MailRead, MailWrite, MailWriteResult];

@NgModule({
  imports: [CommonModule, RouterModule, SharedModule, MiddleUIModule],
  declarations: DECLARATIONS,
  exports: DECLARATIONS
})
export class MailRWModule { }
