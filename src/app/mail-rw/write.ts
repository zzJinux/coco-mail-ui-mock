import { Component } from '@angular/core';
@Component({
  selector: 'mail-write',
  templateUrl: './write.html',
  styleUrls: ['./write.css'],
  preserveWhitespaces: false
})
export class MailWrite {
  saved_time = new Date;
  param = {
    encoding: 'auto',
    tome: false,
    from_email: ''
  };
  email_opts = [
    { name: 'adsf@gmail.com', code: 'adsf@gmail.com' },
    { name: 'asdfasdfdf@gmail.com', code: 'asdfasdfdf@gmail.com' }
  ];
  _css = {};
  encoding_opts = [
    {
      'code': 'auto',
      'name': '자동선택'
    },
    {
      'code': 'utf-8',
      'name': '다국어'
    },
    {
      'code': 'euc-kr',
      'name': '한국어'
    },
    {
      'code': 'shift-jis',
      'name': '일본어'
    },
    {
      'code': 'gb2312',
      'name': '중국어(간체)'
    },
    {
      'code': 'big5',
      'name': '중국어(번체)'
    }
  ];

  show_cc_bcc = true;

  send_option_opened = true;

  change_tome() {
  }

  toggleCcBcc() {
    this.show_cc_bcc = !this.show_cc_bcc;
  }
}
