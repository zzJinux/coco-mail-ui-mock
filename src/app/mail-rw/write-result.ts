import { Component } from '@angular/core';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mail-write-result',
  templateUrl: './write-result.html',
  styleUrls: ['./write-result.scss'],
  preserveWhitespaces: false
})
// tslint:disable-next-line:component-class-suffix
export class MailWriteResult {
  bean = {};
  rcv_list = [
    {name: '물병옥', exist: false, email: 'sookwook2@naver.com'},
    {name: '물병옥', exist: true, email: 'sookwook2@naver.com'},
    {name: '물병옥', exist: false, email: 'sookwook2@naver.com'},
    {name: '물병옥', exist: true, email: 'sookwook2@naver.com'}
  ];
}
