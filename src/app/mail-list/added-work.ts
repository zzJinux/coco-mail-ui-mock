import { Component, Input } from '@angular/core';

/*  */import { ElementRef } from '@angular/core';
/*  */import { BsDropdownDirective } from 'ngx-bootstrap/dropdown';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mail-list__added-work, [fuckIe]',
  templateUrl: './added-work.html',
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    '(mouseenter)': '__activated = true',
    '(mouseleave)': '__activated = false'
  }
})
// tslint:disable-next-line:class-name component-class-suffix
export class MailList_AddedWork {
  @Input() sn: number;
  __activated: boolean;

  constructor(dropdown: BsDropdownDirective, elRef: ElementRef) {
    this.__dropdownWorkaround(dropdown, elRef);
  }

  /* https://github.com/valor-software/ngx-bootstrap/issues/2172 workaround */
  __dropdownWorkaround(dropdown: BsDropdownDirective, elRef: ElementRef) {
    const handler = (event: MouseEvent) => {
      if (dropdown.autoClose && event.button !== 2 && !elRef.nativeElement.contains(event.target)) {
        dropdown.hide();
      }
    };
    dropdown.isOpenChange.subscribe(isOpen => {
      if (!isOpen) {
        document.removeEventListener('click', handler);
        return;
      }
      document.addEventListener('click', handler);
    });
  }

  __() {
    console.log('hi');
  }

}
