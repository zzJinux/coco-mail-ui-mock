import { Component, ViewContainerRef, Input } from '@angular/core';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mail-list__normal',
  templateUrl: './normal-list.html',
  styleUrls: ['./normal-list.scss'],
  preserveWhitespaces: false
})
// tslint:disable-next-line:component-class-suffix class-name
export class MailList_Normal {
  @Input() mode = 0;
}
