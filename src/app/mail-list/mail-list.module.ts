import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared.module';

import { MailList_Normal } from './normal-list';
import { MailList_Vertical } from './vertical-list';
import { MailList_Horizon } from './horizon-list';
import { MailList_Header } from './header';
import { MailList_View } from './view';
import { MailList_AddedWork } from './added-work';
import { MailList_Readok } from './readok';

import { MiddleUIModule } from '../middle-ui';
import { MailRWModule } from '../mail-rw/mail-rw.module';

const DECLARATIONS = [
  MailList_Normal,
  MailList_Vertical,
  MailList_Horizon,
  MailList_Header,
  MailList_View,
  MailList_AddedWork,
  MailList_Readok
];

@NgModule({
  imports: [CommonModule, RouterModule, SharedModule, MiddleUIModule, MailRWModule],
  declarations: DECLARATIONS,
  exports: DECLARATIONS
})
export class MailListModule { }
