import { Component, OnInit, ViewContainerRef, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { DataSource } from '@angular/cdk/collections';

import { MailItem, MAILS_TEST } from '../mock-data/mail-raw';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mail-list__view',
  templateUrl: './view.html',
  styleUrls: ['./view.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false
})
// tslint:disable-next-line:class-name component-class-suffix
export class MailList_View implements OnInit {

  displayedColumns = ['checked', 'from', 'subject', 'time', 'etc'];
  dataSource = new MailListDataSource();

  s_mailbox = 1100;

  ngOnInit() {
    MAILS_TEST[0].from_email = 'asdfasdfsdf34@naver.com';
    MAILS_TEST[1].from_name = '머머리';
    this.dataSource.reloadList(MAILS_TEST);
  }

  clickChecked() {
    console.log('call clickChecked');
  }

  changeChecked() {
    console.log('call changeChecked');
  }

  goReadmailLink() {
    console.log('call goReadmailLink');
  }

  openPreviewMail() {
    console.log('call openPreviewMail');
  }

  openPopupMail() {
    console.log('call openPopupMail');
  }

  openReplyPopup() { }
  openReplyAllPopup() { }
  openForwardPopup() { }
  setPriorityMail() { }
}

export class MailListDataSource extends DataSource<MailItem> {

  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<MailItem[]> = new BehaviorSubject<MailItem[]>([]);
  get data(): MailItem[] { return this.dataChange.value; }

  constructor() {
    super();
    // this.dataChange.next(mail_list);
  }

  connect(): Observable<MailItem[]> {
    return this.dataChange;
  }

  disconnect() {

  }

  getData(): MailItem[] {
    return this.data;
  }

  reloadList(mail_list: MailItem[]) {
    this.dataChange.next(mail_list);
  }

}
