import { Component, ViewChild } from '@angular/core';
import { MovecopyPopup } from '../middle-ui/movecopy-popup';

@Component({
  selector: 'mail-list__header',
  templateUrl: './header.html',
  // styleUrls: ['./header.css'],
  host: {
    'class': 'clearfix'
  },
  preserveWhitespaces: false
})
export class MailList_Header {

  checkAll: boolean;
  MAIL_LISTING_TYPE: string;
  personbox_toggle: boolean;

  @ViewChild(MovecopyPopup) movecopyPopup: MovecopyPopup;

  isTrash = false;
  isTemporary = false;
  isSent = false;

  openMovecopy(e) {
    this.movecopyPopup.openPopup(e);
  }

  goCheckAllMenu() {}

}
