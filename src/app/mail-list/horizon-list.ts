import { Component, ElementRef, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { BsResizeContainer, BsResizerTrigger } from '../core-widget/resizer';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mail-list__horizon',
  templateUrl: './horizon-list.html',
  styleUrls: ['./horizon-list.scss'],
  preserveWhitespaces: false
})
// tslint:disable-next-line:all
export class MailList_Horizon implements AfterViewInit, OnDestroy {
  @ViewChild('leftPanel') leftPanel: ElementRef;
  @ViewChild('rightPanel') rightPanel: ElementRef;
  @ViewChild(BsResizerTrigger) ownResizer: BsResizerTrigger;
  leftWidthCent = 50;

  _last = false;
  _lastLeftWidth: number;
  _lastRightWidth: number;

  // parentResizer: BsResizerTrigger;
  // sbscrpt: any;

  constructor(/* _resizeContainer: BsResizeContainer */) {
    // this.parentResizer = _resizeContainer.resizeTrigger;
  }

  ngAfterViewInit() {
    // this.sbscrpt = this.parentResizer.bsResizerTrigger.subscribe(value => {
    //   this.ownResizer._onPan(-value.deltaX, -value.deltaY, value.type);
    // });
  }

  ngOnDestroy() {
    // this.sbscrpt.unsubscribe();
  }

  _resizePanels(deltaX: number | null) {
    if (deltaX === null) {
      this._last = false;
      return;
    }

    const left = <HTMLElement>this.leftPanel.nativeElement;
    const right = <HTMLElement>this.rightPanel.nativeElement;

    if (!this._last) {
      this._last = true;
      this._lastLeftWidth = left.offsetWidth;
      this._lastRightWidth = right.offsetWidth;
    }

    left.style.width = `${this._lastLeftWidth + deltaX}px`;
    right.style.width = `${this._lastRightWidth - deltaX - 1}px`;

    // const appliedLeftWidth = Math.max(left.offsetWidth, this._lastLeftWidth + deltaX);
    // const appliedRightWidth = Math.max(right.offsetWidth, this._lastRightWidth - deltaX);

    // if (deltaX < 0) { deltaX = Math.max(appliedLeftWidth - this._lastLeftWidth, this._lastRightWidth - appliedRightWidth); }
    // // tslint:disable-next-line:one-line
    // else { deltaX = Math.min(appliedLeftWidth - this._lastLeftWidth, this._lastRightWidth - appliedRightWidth); }

    // left.style.width = `${this._lastLeftWidth + deltaX}px`;
    // right.style.width = `${this._lastRightWidth - deltaX}px`;
  }

}
