import { Component, OnInit, ViewContainerRef, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { DataSource } from '@angular/cdk/collections';

import { ReakokItem, TEST_SET1, TEST_SET2 } from '../mock-data/readok-raw';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mail-list__readok',
  templateUrl: './readok.html',
  styleUrls: ['./readok.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false
})
// tslint:disable-next-line:class-name component-class-suffix
export class MailList_Readok implements OnInit {

  displayedColumns = ['checked', 'subject', 'to'];
  dataSource = new MailListDataSource();

  s_mailbox = 1100;

  ngOnInit() {
    const asdf = new Date;
    const test_set = [...TEST_SET1, ...TEST_SET2];
    for (const item of test_set) {
      for (const iii of item.list) {
        Object.assign(iii, {
          send_time: asdf,
          reserve_time: asdf,
          readok_time: asdf,
          cancel_time: asdf
        });
      }
    }
    this.dataSource.reloadList(test_set);
  }

  clickChecked() {
    console.log('call clickChecked');
  }

  cancelWaiting() { }
  cancelReserved() { }
  cancelSending() { }
  toggleReadokList(item: any) {
    item.show = !item.show;
    return false;
  }
}

export class MailListDataSource extends DataSource<ReakokItem> {

  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<ReakokItem[]> = new BehaviorSubject<ReakokItem[]>([]);
  get data(): ReakokItem[] { return this.dataChange.value; }

  constructor() {
    super();
    // this.dataChange.next(mail_list);
  }

  connect(): Observable<ReakokItem[]> {
    return this.dataChange;
  }

  disconnect() {

  }

  getData(): ReakokItem[] {
    return this.data;
  }

  reloadList(mail_list: ReakokItem[]) {
    this.dataChange.next(mail_list);
  }

}
