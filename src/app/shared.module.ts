import { NgModule, Pipe, PipeTransform, Directive, Input } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CoreWidgetModule } from './core-widget';
import { TreeModule } from 'angular-tree-component';
import { SharedCdkModule } from './shared-cdk';
import { SharedBootstrapModule } from './shared-bootstrap';
import { CocoPipeModule } from './coco-pipe.module';

@Directive({
  selector: '[focusing]'
})
export class FocusingFake {
  @Input() focusing: any;
}

@NgModule({
  imports: [
    FormsModule,
    CocoPipeModule,
    CoreWidgetModule,
    SharedCdkModule,
    SharedBootstrapModule,
    TreeModule
  ],
  declarations: [FocusingFake],
  exports: [
    FormsModule,
    CocoPipeModule,
    CoreWidgetModule,
    SharedCdkModule,
    SharedBootstrapModule,
    TreeModule,
    FocusingFake
  ]
})
export class SharedModule { }
