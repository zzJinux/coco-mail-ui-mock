import { OnInit, ViewChild, TemplateRef, ViewContainerRef, Input } from '@angular/core';

export class HostlessComponentBase implements OnInit {
  @ViewChild('_root') protected _rootTemplate: TemplateRef<any>;
  @Input() __enclosed: boolean;

  constructor(private vcr: ViewContainerRef) { }

  ngOnInit() {
    if (!this.__enclosed) { this.vcr.createEmbeddedView(this._rootTemplate); }
  }

}
