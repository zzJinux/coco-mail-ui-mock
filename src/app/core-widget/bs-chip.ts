import { Directive, Input, ElementRef, OnChanges } from '@angular/core';

/* styles on global */

@Directive({
  selector: '[bsChip]',
  host: {
    'class': 'bs-chip btn'
  }
})
export class BsChip implements OnChanges {

  @Input('bsChip') theme =  'default';

  constructor(private elementRef: ElementRef) {
    this.updateClass(`btn-${this.theme}`);
  }

  ngOnChanges(ch) {
    const _old = `btn-${ch.theme.previousValue}`;
    const _new = `btn-${ch.theme.currentValue}`;
    this.updateClass(_new, _old);
  }

  updateClass(_new: string, _old: string = '.') {
    const classList = this.elementRef.nativeElement.classList;
    classList.add(_new);
    classList.remove(_old);
  }

}
