import { Directive, Optional } from '@angular/core';
import { NgForm, FormGroupDirective, AbstractControl } from '@angular/forms';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: 'bs-error',
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    'class': 'bs-error alert alert-danger',
    '[class.hidden]': 'control && (control.valid || control.pristine) || null',
    'role': 'alert'
  }
})
// tslint:disable-next-line:directive-class-suffix
export class BsError {

  control: AbstractControl | null;

  constructor(
    @Optional() _parentForm: NgForm,
    @Optional() _parentFormGroup: FormGroupDirective,
  ) {
    this.control = (_parentForm && _parentForm.control) || (_parentFormGroup && _parentFormGroup.control);
  }
}
