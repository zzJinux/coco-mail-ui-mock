/* original: https://github.com/angular/material2/blob/master/src/lib/checkbox/checkbox-required-validator.ts */

import {
  Directive,
  forwardRef,
  Provider,
} from '@angular/core';
import {
  CheckboxRequiredValidator,
  NG_VALIDATORS,
} from '@angular/forms';

export const BS_CHECKBOX_REQUIRED_VALIDATOR: Provider = {
  provide: NG_VALIDATORS,
  useExisting: forwardRef(() => BsCheckboxRequiredValidator),
  multi: true
};

/**
 * Validator for Material checkbox's required attribute in template-driven checkbox.
 * Current CheckboxRequiredValidator only work with `input type=checkbox` and does not
 * work with `bs-checkbox`.
 */
@Directive({
  selector: `bs-checkbox[required][formControlName],
             bs-checkbox[required][formControl], bs-checkbox[required][ngModel]`,
  providers: [BS_CHECKBOX_REQUIRED_VALIDATOR],
  host: { '[attr.required]': 'required ? "" : null' }
})
export class BsCheckboxRequiredValidator extends CheckboxRequiredValidator { }
