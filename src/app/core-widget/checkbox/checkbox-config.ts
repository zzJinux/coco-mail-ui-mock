/* original: https://github.com/angular/material2/blob/master/src/lib/checkbox/checkbox-config.ts */

import { InjectionToken } from '@angular/core';


/**
 * Checkbox click action when user click on input element.
 * noop: Do not toggle checked or indeterminate.
 * check: Only toggle checked status, ignore indeterminate.
 * check-indeterminate: Toggle checked status, set indeterminate to false. Default behavior.
 * undefined: Same as `check-indeterminate`.
 */
export type BsCheckboxClickAction = 'noop' | 'check' | 'check-indeterminate' | undefined;

/**
 * Injection token that can be used to specify the checkbox click behavior.
 */
export const BS_CHECKBOX_CLICK_ACTION =
  new InjectionToken<BsCheckboxClickAction>('bs-checkbox-click-action');