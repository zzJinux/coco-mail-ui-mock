/* https://github.com/angular/material2/blob/master/src/lib/checkbox/checkbox-module.ts */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ObserversModule } from '@angular/cdk/observers';
import { BsCheckbox } from './checkbox';
import { BsCheckboxRequiredValidator } from './checkbox-required-validator';
import { A11yModule } from '@angular/cdk/a11y';

@NgModule({
  imports: [CommonModule, ObserversModule, A11yModule],
  exports: [BsCheckbox, BsCheckboxRequiredValidator],
  declarations: [BsCheckbox, BsCheckboxRequiredValidator],
})
export class BsCheckboxModule { }