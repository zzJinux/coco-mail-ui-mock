import { Directive, OnInit, Input, HostBinding, Optional, ElementRef } from '@angular/core';
import { FormControl, NgModel, FormControlName } from '@angular/forms';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: 'input[placeholder]'
})
export class PlaceholderDirective implements OnInit {

  @Input() placeholder;
  inputElement: HTMLInputElement;
  get isValuePropertyEmpty() {
    return !this.inputElement.value;
  }
  @HostBinding('class.placeholder-shown')
  isPlaceholderActive: boolean;

  _isTypePassword: boolean;

  constructor(
    @Optional() private formControl: FormControl,
    @Optional() private ngModel: NgModel,
    @Optional() private formControlName: FormControlName,
    elementRef: ElementRef
  ) {
    this.inputElement = elementRef.nativeElement;
    this._isTypePassword = this.inputElement.type === 'password';
  }

  ngOnInit() {
    if (document['documentMode'] === 9) {
      if (!this.formControl && this.ngModel) {
        this.formControl = this.ngModel.control;
      }
      if (!this.formControl && this.formControlName) {
        this.formControl = this.formControlName.control;
      }
      if (this.formControl) {
        this.formControl.valueChanges.subscribe(() => {
          this.updateOnChange();
        });
      }

      this.updateOnChange();

      this.inputElement.addEventListener('blur', () => {
        if (this.isValuePropertyEmpty) {
          this.setPlaceholder();
        }
      });
      this.inputElement.addEventListener('focus', () => {
        if (this.isPlaceholderActive) {
          this.clearPlaceholder();
        }
      });
    }
  }

  updateOnChange() {
    if (this.isValuePropertyEmpty && document.activeElement !== this.inputElement) {
      this.setPlaceholder();
    } else {
      this.deactivatePlaceholder();
    }
  }

  setPlaceholder() {
    this.activatePlaceholder();
    this.inputElement.value = this.placeholder;
  }

  clearPlaceholder() {
    this.deactivatePlaceholder();
    this.inputElement.value = '';
  }

  activatePlaceholder() {
    this.isPlaceholderActive = true;
    if (this.inputElement.type === 'password') {
      this.inputElement.type = 'text';
    }
  }

  deactivatePlaceholder() {
    this.isPlaceholderActive = false;
    if (this._isTypePassword && this.inputElement.type !== 'password') {
      this.inputElement.type = 'password';
    }
  }
}
