/* original: https://github.com/angular/material2/blob/master/src/lib/core/common-behaviors/constructor.ts */

/** @docs-private */
export type Constructor<T> = new(...args: any[]) => T;