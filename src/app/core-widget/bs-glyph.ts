import { Component, OnChanges, Input, HostBinding, ElementRef, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: '[bsGlyph]',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BsGlyph implements OnChanges {

  @Input('bsGlyph') glyphName: string;
  @HostBinding('class.glyphicon') private _baseClass = true;

  constructor(private elemRef: ElementRef) { }

  ngOnChanges(ch) {
    const _old = `glyphicon-${ch.glyphName.previousValue}`;
    const _new = `glyphicon-${ch.glyphName.currentValue}`;
    const classList = this.elemRef.nativeElement.classList;
    classList.remove(_old);
    classList.add(_new);
  }

}
