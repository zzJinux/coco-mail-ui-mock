import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PortalModule } from '@angular/cdk/portal';
import { BsDropdownModule } from 'ngx-bootstrap';
import { BsOption, BsSelect } from './select';

@NgModule({
    imports: [CommonModule, FormsModule, BsDropdownModule, PortalModule],
    declarations: [BsOption, BsSelect],
    exports: [BsOption, BsSelect]
})
export class BsSelectModule { }
