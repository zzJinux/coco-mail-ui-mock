/* tslint:disable:member-ordering */

import {
  Component,
  ViewChild,
  ContentChildren,
  QueryList,
  Input,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  OnInit,
  AfterContentInit,
  DoCheck,
  OnChanges,
  OnDestroy,
  ElementRef,
  AfterViewChecked,
  SimpleChanges,
  NgZone,
  Self,
  Optional,
  isDevMode,
  TemplateRef,
  ViewContainerRef,
  ViewEncapsulation
} from '@angular/core';
import {
  ControlValueAccessor,
  NG_VALUE_ACCESSOR,
  NgControl
} from '@angular/forms';
import { CdkPortal } from '@angular/cdk/portal';
import { SelectionModel } from '@angular/cdk/collections';
import { Subject } from 'rxjs/Subject';
import { merge } from 'rxjs/observable/merge';
import { defer } from 'rxjs/observable/defer';
import { startWith, takeUntil, filter, take, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';

/*  */import { BsDropdownDirective } from 'ngx-bootstrap';

export class BsOptionSelectionChange {
  constructor(
    public source: BsOption,
    public isUserInput = false) { }
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'bs-option',
  template: `
    <ng-template #tpl>
      <li role="menuitem"
          [class.active]="selected"
          [class.disabled]="disabled"
          (click)="_selectViaInteraction()">
        <a #hostElement class="dropdown-item" (click)="false">
          <ng-content></ng-content>
        </a>
      </li>
    </ng-template>
  `,
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush
})
// tslint:disable-next-line:component-class-suffix
export class BsOption implements OnInit, AfterViewChecked {

  @Input() listMaxHeight: string;

  @ViewChild('hostElement') private _element: ElementRef;

  private _selected: boolean;
  // private _active: boolean;
  private _mostRecentViewValue = '';

  get selected(): boolean { return this._selected; }
  // get active(): boolean { return this._active; }
  @Input() value: any;
  @Input() disabled: boolean;

  get viewValue(): string {
    // TODO(kara): Add input property alternative for node envs.
    return (this._getHostElement().textContent || '').trim();
  }

  @Output() readonly onSelectionChange = new EventEmitter<BsOptionSelectionChange>();

  readonly _stateChanges = new Subject<void>();

  @ViewChild('tpl') tpl: TemplateRef<any>;
  // @ViewChild(CdkPortal) public portal: CdkPortal;

  constructor(
    private _vcr: ViewContainerRef,
    private _changeDetectorRef: ChangeDetectorRef) { }

  select(): void {
    this._selected = true;
    this._changeDetectorRef.markForCheck();
    this._emitSelectionChangeEvent();
  }

  deselect(): void {
    this._selected = false;
    this._changeDetectorRef.markForCheck();
    this._emitSelectionChangeEvent();
  }

  // setActiveStyles(): void {
  //   if (!this._active) {
  //     this._active = true;
  //     this._changeDetectorRef.markForCheck();
  //   }
  // }

  // setInactiveStyles(): void {
  //   if (this._active) {
  //     this._active = false;
  //     this._changeDetectorRef.markForCheck();
  //   }
  // }

  _selectViaInteraction(): void {
    if (!this.disabled) {
      // this._selected = this.multiple ? !this._selected : true;
      this._selected = true;
      this._changeDetectorRef.markForCheck();
      this._emitSelectionChangeEvent(true);
    }
  }

  _getHostElement(): HTMLElement {
    return this._element.nativeElement;
  }

  ngOnInit() {
    this._vcr.createEmbeddedView(this.tpl);
  }

  ngAfterViewChecked() {
    // Since parent components could be using the option's label to display the selected values
    // (e.g. `mat-select`) and they don't have a way of knowing if the option's label has changed
    // we have to check for changes in the DOM ourselves and dispatch an event. These checks are
    // relatively cheap, however we still limit them only to selected options in order to avoid
    // hitting the DOM too often.
    if (this._selected) {
      const viewValue = this.viewValue;

      if (viewValue !== this._mostRecentViewValue) {
        this._mostRecentViewValue = viewValue;
        this._stateChanges.next();
      }
    }
  }

  private _emitSelectionChangeEvent(isUserInput = false): void {
    this.onSelectionChange.emit(new BsOptionSelectionChange(this, isUserInput));
  }

}

export class BsSelectChange {
  constructor(
    public source: BsSelect,
    public value: any) { }
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'bs-select',
  templateUrl: './select.html',
  styleUrls: ['./select.scss'],
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    '[class.bs-select]': 'true',
    /* https://github.com/valor-software/ngx-bootstrap/issues/2172 workaround */
    /* */ '(mouseenter)': '__activated = true',
    /* */ '(mouseleave)': '__activated = false'
  },
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush
})
// tslint:disable-next-line:component-class-suffix
export class BsSelect implements OnInit, AfterContentInit, DoCheck, OnChanges, OnDestroy, ControlValueAccessor {

   /* https://github.com/valor-software/ngx-bootstrap/issues/2172 workaround */
   __activated: boolean;
   @ViewChild(BsDropdownDirective) __dropdown: BsDropdownDirective;

  @Input() listHeight: string;

  readonly stateChanges = new Subject<void>();

  private _placeholder: string;

  private _compareWith = (o1: any, o2: any) => o1 === o2;

  /** Emits whenever the component is destroyed. */
  private readonly _destroy = new Subject<void>();

  _selectionModel: SelectionModel<BsOption>;

  _onChange: (value: any) => void = () => { };

  /** Panel containing the select options. */
  @ViewChild('panel') panel: ElementRef;

  @ContentChildren(BsOption) options: QueryList<BsOption>;

  @Input() disabled: boolean;

  @Input()
  get placeholder(): string { return this._placeholder; }
  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }

  /** Value of the select control. */
  @Input()
  get value(): any { return this._value; }
  set value(newValue: any) {
    if (newValue !== this._value) {
      this.writeValue(newValue);
      this._value = newValue;
    }
  }
  private _value: any;

  /** Combined stream of all of the child options' change events. */
  readonly optionSelectionChanges: Observable<BsOptionSelectionChange> = defer(() => {
    if (this.options) {
      return merge(...this.options.map(option => option.onSelectionChange));
    }

    return this._ngZone.onStable
      .asObservable()
      .pipe(take(1), switchMap(() => this.optionSelectionChanges));
  });

  @Output() readonly selectionChange = new EventEmitter<BsSelectChange>();

  @Output() readonly valueChange = new EventEmitter<any>();

  constructor(
    private _ngZone: NgZone,
    private _changeDetectorRef: ChangeDetectorRef,
    @Self() @Optional() public ngControl: NgControl,
    /* https://github.com/valor-software/ngx-bootstrap/issues/2172 workaround */
    private elRef: ElementRef
  ) {
    if (this.ngControl) {
      // Note: we provide the value accessor through here, instead of
      // the `providers` to avoid running into a circular import.
      this.ngControl.valueAccessor = this;
    }
  }

  ngOnInit() {
    this._selectionModel = new SelectionModel<BsOption>(false, undefined, false);
    this.stateChanges.next();

    /* https://github.com/valor-software/ngx-bootstrap/issues/2172 workaround */
    this.__dropdownWorkaround(this.__dropdown, this.elRef);
  }

  ngAfterContentInit() {
    // this._initKeyManager();

    this.options.changes.pipe(startWith(null), takeUntil(this._destroy)).subscribe(() => {
      this._resetOptions();
      this._initializeSelection();
    });
  }

  ngDoCheck() {
    // if (this.ngControl) {
    //   this.updateErrorState();
    // }
  }

  ngOnChanges(changes: SimpleChanges) {
    // Updating the disabled state is handled by `mixinDisabled`, but we need to additionally let
    // the parent form field know to run change detection when the disabled state changes.
    if (changes.disabled) {
      this.stateChanges.next();
    }
  }

  ngOnDestroy() {
    this._destroy.next();
    this._destroy.complete();
    this.stateChanges.complete();
  }

  writeValue(value: any) {
    if (this.options) {
      this._setSelectionByValue(value);
    }
  }

  registerOnChange(fn: (value: any) => void) {
    this._onChange = fn;
  }

  registerOnTouched(fn: (value: any) => void) { }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
    this._changeDetectorRef.markForCheck();
    this.stateChanges.next();
  }

  /** The currently selected option. */
  get selected(): BsOption {
    return this._selectionModel.selected[0];
  }

  get triggerValue(): string {
    if (this.empty) {
      return '';
    }

    return this._selectionModel.selected[0].viewValue;
  }

  _onAttached(): void {
    // TODO: scroll to active option
    // this.panel.nativeElement.scrollTop = this._scrollTop;
  }

  get empty(): boolean {
    return !this._selectionModel || this._selectionModel.isEmpty();
  }

  private _initializeSelection(): void {
    // Defer setting the value in order to avoid the "Expression
    // has changed after it was checked" errors from Angular.
    Promise.resolve().then(() => {
      this._setSelectionByValue(this.ngControl ? this.ngControl.value : this._value);
    });
  }

  /**
   * Sets the selected option based on a value. If no option can be
   * found with the designated value, the select trigger is cleared.
   */
  private _setSelectionByValue(value: any | any[], isUserInput = false): void {
    this._clearSelection();

    const correspondingOption = this._selectValue(value, isUserInput);

    this._changeDetectorRef.markForCheck();
  }

  private _selectValue(value: any, isUserInput = false): BsOption | undefined {
    const correspondingOption = this.options.find((option: BsOption) => {
      try {
        // Treat null as a special reset value.
        return option.value != null && this._compareWith(option.value, value);
      } catch (error) {
        if (isDevMode()) {
          // Notify developers of errors in their comparator.
          console.warn(error);
        }
        return false;
      }
    });

    if (correspondingOption) {
      isUserInput ? correspondingOption._selectViaInteraction() : correspondingOption.select();
      this._selectionModel.select(correspondingOption);
      this.stateChanges.next();
    }

    return correspondingOption;
  }

  /** Drops current option subscriptions and IDs and resets from scratch. */
  private _resetOptions(): void {
    const changedOrDestroyed = merge(this.options.changes, this._destroy);

    this.optionSelectionChanges
      .pipe(takeUntil(changedOrDestroyed), filter(event => event.isUserInput))
      .subscribe(event => {
        this._onSelect(event.source);

        // if (this._panelOpen) {
        //   this.close();
        //   this.focus();
        // }
      });

    // Listen to changes in the internal state of the options and react accordingly.
    // Handles cases like the labels of the selected options changing.
    merge(...this.options.map(option => option._stateChanges))
      .pipe(takeUntil(changedOrDestroyed))
      .subscribe(() => {
        this._changeDetectorRef.markForCheck();
        this.stateChanges.next();
      });

    // this._setOptionIds();
  }

  private _clearSelection(skip?: BsOption): void {
    this._selectionModel.clear();
    this.options.forEach(option => {
      if (option !== skip) {
        option.deselect();
      }
    });
    this.stateChanges.next();
  }

  /** Invoked when an option is clicked. */
  private _onSelect(option: BsOption): void {
    const wasSelected = this._selectionModel.isSelected(option);

    {
      this._clearSelection(option.value == null ? undefined : option);

      if (option.value == null) {
        this._propagateChanges(option.value);
      } else {
        this._selectionModel.select(option);
        this.stateChanges.next();
      }
    }

    if (wasSelected !== this._selectionModel.isSelected(option)) {
      this._propagateChanges();
    }
  }

  /** Emits change event to set the model value. */
  private _propagateChanges(fallbackValue?: any): void {
    let valueToEmit: any = null;

    valueToEmit = this.selected ? (this.selected as BsOption).value : fallbackValue;

    this._value = valueToEmit;
    this.valueChange.emit(valueToEmit);
    this._onChange(valueToEmit);
    this.selectionChange.emit(new BsSelectChange(this, valueToEmit));
    this._changeDetectorRef.markForCheck();
  }

  /* https://github.com/valor-software/ngx-bootstrap/issues/2172 workaround */
  __dropdownWorkaround(dropdown: BsDropdownDirective, elRef: ElementRef) {
    const handler = (event: MouseEvent) => {
      if (dropdown.autoClose && event.button !== 2 && !elRef.nativeElement.contains(event.target)) {
        dropdown.hide();
      }
    };
    dropdown.isOpenChange.subscribe(isOpen => {
      if (!isOpen) {
        document.removeEventListener('click', handler);
        return;
      }
      document.addEventListener('click', handler);
    });
  }
}
