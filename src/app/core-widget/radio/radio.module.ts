/* original: https://github.com/angular/material2/blob/master/src/lib/radio/radio-module.ts */

import { A11yModule } from '@angular/cdk/a11y';
import { UNIQUE_SELECTION_DISPATCHER_PROVIDER } from '@angular/cdk/collections';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BsRadioButton, BsRadioGroup } from './radio';

@NgModule({
  imports: [CommonModule, A11yModule],
  exports: [BsRadioGroup, BsRadioButton],
  providers: [UNIQUE_SELECTION_DISPATCHER_PROVIDER],
  declarations: [BsRadioGroup, BsRadioButton],
})
export class BsRadioModule { }
