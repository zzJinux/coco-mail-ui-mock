import { Directive, OnInit, ElementRef, InjectionToken, forwardRef } from '@angular/core';
import { BsResizeContainer } from './resize-container';
import { Resizable } from './resizable-def';
import { BsResizerTrigger } from './resizer';
import {
  OffsetWidth,
  OffsetHeight,
  OffsetWidthSetter,
  OffsetHeightSetter,
  MinWidth,
  MaxWidth,
  MinHeight,
  MaxHeight
} from './size-measures';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[bsResizable]',
  exportAs: 'bsResizable',
  // providers: [{ provide: RESIZABLE_TOKEN, useExisting: forwardRef(() => BsResizable) }]
})
// tslint:disable-next-line:directive-class-suffix
export class BsResizable implements Resizable, OnInit {

  element: HTMLElement;
  private getOffsetLength: (element: HTMLElement) => number;
  private setOffsetLength: (element: HTMLElement, value: number | string) => void;
  private _minSize: number;
  private _maxSize: number;

  get targetSize() {
    return this.getOffsetLength(this.element);
  }
  set targetSize(value: number) {
    this.setOffsetLength(this.element, value);
  }
  set targetSizeCalc(value: string) {
    this.setOffsetLength(this.element, value);
  }
  get minSize() {
    return this._minSize;
  }
  get maxSize() {
    return this._maxSize;
  }

  nearTriggers: [BsResizerTrigger, BsResizerTrigger];

  _lastSize: number;

  constructor(_elementRef: ElementRef, private _conatiner: BsResizeContainer) {
    this.element = _elementRef.nativeElement;
    _conatiner.resizables.push(this);
  }

  ngOnInit() {
    [this.getOffsetLength, this.setOffsetLength, this._minSize, this._maxSize] = this._conatiner.axis === 'x' ?
      [OffsetWidth, OffsetWidthSetter, MinWidth(this.element), MaxWidth(this.element)] :
      [OffsetHeight, OffsetHeightSetter, MinHeight(this.element), MaxHeight(this.element)];
  }

}
