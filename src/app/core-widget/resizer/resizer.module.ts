import { NgModule, Injectable } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HAMMER_GESTURE_CONFIG, HammerGestureConfig } from '@angular/platform-browser';

import { BsResizeContainer } from './resize-container';
import { BsResizable } from './resizable';
import { BsResizerTrigger } from './resizer';

@Injectable()
export class GestureConfig extends HammerGestureConfig {

  buildHammer(element: HTMLElement) {
    const mc = new Hammer.Manager(element);
    const pan = new Hammer.Pan({ threshold: 0 });
    mc.add(pan);

    return mc;
  }
}

@NgModule({
  imports: [CommonModule],
  declarations: [BsResizerTrigger, BsResizable, BsResizeContainer],
  exports: [BsResizerTrigger, BsResizable, BsResizeContainer],
  providers: [{ provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig }]
})
export class BsResizerModule { }
