import { Directive, Input, Output, ElementRef, OnInit, HostListener, EventEmitter } from '@angular/core';
import { BsResizeContainer } from './resize-container';
import { Resizable } from './resizable-def';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[bsResizerTrigger]',
  exportAs: 'bsResizerTrigger',
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    'class': 'bs-resizer-trigger'
  }
})
// tslint:disable-next-line:directive-class-suffix
export class BsResizerTrigger {

  private _isLastSet = false;

  resizables: Resizable[];
  @Output() bsResizerTrigger = new EventEmitter<{ deltaX: number, deltaY: number, type: string }>();

  constructor(private _container: BsResizeContainer) {
    this.resizables = _container.resizables;
    _container.resizeTrigger = this;
    // const parent = _container.parentResizable;

    // this.resizables[0].nearTriggers = [
    //   parent ? parent.nearTriggers[0] : null,
    //   this
    // ];
    // this.resizables[1].nearTriggers = [
    //   this,
    //   parent ? parent.nearTriggers[1] : null
    // ];
  }

  /* tslint:disable:no-bitwise */
  @HostListener('panmove', ['$event.deltaX', '$event.deltaY', '$event.type'])
  @HostListener('panend', ['$event.deltaX', '$event.deltaY', '$event.type'])
  _onPan(deltaX: number, deltaY: number, type: string) {
    /* DIRECTION_HORIZONTAL: 6 */
    /* DIRECTION_VERTICAL: 24 */
    if (this._container.axis === 'x') {
      this._handleResize(deltaX);
      this.bsResizerTrigger.emit({ deltaX, deltaY, type });
    } else {
      this._handleResize(deltaY);
      this.bsResizerTrigger.emit({ deltaX, deltaY, type });
    }

    if (type === 'panend') {
      this._handleResize(null);
      this.bsResizerTrigger.emit({ deltaX, deltaY, type });
    }
  }

  private _handleResize(delta: number | null) {
    const one = this.resizables[0], two = this.resizables[1];

    if (delta === null) {
      this._isLastSet = false;
      one.targetSizeCalc = `100% - ${two.targetSize + 0.6}px`;
      return;
    }

    if (!this._isLastSet) {
      this._isLastSet = true;
      one._lastSize = one.targetSize;
      two._lastSize = two.targetSize;
    }

    delta = Math.min(Math.max(one._lastSize + delta, one.minSize), one.maxSize) - one._lastSize;
    delta = -Math.min(Math.max(two._lastSize - delta, two.minSize), two.maxSize) + two._lastSize;

    one.targetSize = (one._lastSize + delta) - 0.5;
    two.targetSize = (two._lastSize - delta) - 0.5;
  }
}
