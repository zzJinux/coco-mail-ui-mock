function _Style(element: HTMLElement) {
  return window.getComputedStyle(element);
}

function HorizontalPadding(element: HTMLElement) {
  const style = _Style(element);
  return parseFloat(style.paddingLeft) + parseFloat(style.paddingRight);
}

function VerticalPadding(element: HTMLElement) {
  const style = _Style(element);
  return parseFloat(style.paddingTop) + parseFloat(style.paddingBottom);
}

function HorizontalBorder(element: HTMLElement) {
  const style = _Style(element);
  return parseFloat(style.borderLeftWidth) + parseFloat(style.borderRightWidth);
}

function VerticalBorder(element: HTMLElement) {
  const style = _Style(element);
  return parseFloat(style.borderTopWidth) + parseFloat(style.borderBottomWidth);
}

function ClientWidth(element: HTMLElement) {
  return element.clientWidth;
}

function ClientHeight(element: HTMLElement) {
  return element.clientHeight;
}

function OffsetWidth(element: HTMLElement) {
  return element.offsetWidth;
}

function OffsetHeight(element: HTMLElement) {
  return element.offsetHeight;
}

function OffsetWidthSetter(element: HTMLElement, value: number | string) {
  if (_Style(element).boxSizing !== 'border-box') {
    const adjust = HorizontalPadding(element) + HorizontalBorder(element);
    if (typeof value === 'number') {
      value -= adjust;
    } else {
      value = `${value} - ${adjust}px`;
    }
  }
  if (typeof value === 'number') {
    element.style.width = `${value}px`;
  } else {
    element.style.width = `calc(${value})`;
  }
}

function OffsetHeightSetter(element: HTMLElement, value: number | string) {
  if (_Style(element).boxSizing !== 'border-box') {
    const adjust = VerticalPadding(element) + VerticalBorder(element);
    if (typeof value === 'number') {
      value -= adjust;
    } else {
      value = `${value} - ${adjust}px`;
    }
  }
  if (typeof value === 'number') {
    element.style.height = `${value}px`;
  } else {
    element.style.height = `calc(${value})`;
  }
}

function calcCalc(element: HTMLElement, value: string, relativeTo: 'width' | 'height') {
  if (value.substr(0, 4) !== 'calc') {
    value = `calc(${value})`;
  }

  const another = relativeTo !== 'width' ? 'width' : 'height';
  const ghost = document.createElement('div');
  ghost.setAttribute('style', `position: absolute; left: -16192px; top: -16192px; ${relativeTo}: ${value}; ${another}: 0`);
  element.parentElement.appendChild(ghost);

  const calcuated = relativeTo !== 'width' ? ghost.offsetHeight : ghost.offsetWidth;
  element.parentElement.removeChild(ghost);

  return calcuated;
}

function MinWidth(element: HTMLElement) {
  const styleValue = _Style(element).minWidth;
  return styleValue ? calcCalc(element, styleValue, 'width') : -Infinity;
}

function MaxWidth(element: HTMLElement) {
  const styleValue = _Style(element).maxWidth;
  return styleValue !== 'none' ? calcCalc(element, styleValue, 'width') : Infinity;
}

function MinHeight(element: HTMLElement) {
  const styleValue = _Style(element).minHeight;
  return styleValue ? calcCalc(element, styleValue, 'height') : -Infinity;
}

function MaxHeight(element: HTMLElement) {
  const styleValue = _Style(element).maxHeight;
  return styleValue !== 'none' ? calcCalc(element, styleValue, 'height') : Infinity;
}

export {
  HorizontalPadding,
  VerticalPadding,
  HorizontalBorder,
  VerticalBorder,
  ClientWidth,
  ClientHeight,
  OffsetWidth,
  OffsetHeight,
  OffsetWidthSetter,
  OffsetHeightSetter,
  calcCalc,
  MinWidth,
  MaxWidth,
  MinHeight,
  MaxHeight
};
