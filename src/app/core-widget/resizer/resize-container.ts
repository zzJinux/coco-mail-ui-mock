import { Directive, OnInit, Input, ElementRef, Optional, Inject } from '@angular/core';
import { Resizable, RESIZABLE_TOKEN } from './resizable-def';
import { BsResizerTrigger } from './resizer';
import { ClientWidth, ClientHeight, HorizontalPadding, VerticalPadding } from './size-measures';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[bsResizeContainer]',
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    'class': 'bs-resize-container',
    '[class.bs-resizer-x]': 'axis === "x"',
    '[class.bs-resizer-y]': 'axis === "y"'
  }
})
// tslint:disable-next-line:directive-class-suffix
export class BsResizeContainer implements OnInit {

  @Input() axis: 'x' | 'y' = 'x';

  element: HTMLElement;
  resizables: Resizable[] = [];
  resizeTrigger: BsResizerTrigger;
  private fnPaddingEdgeLength: (element: HTMLElement) => number;
  private fnPaddingLength: (element: HTMLElement) => number;

  get contentLength() {
    return this.fnPaddingEdgeLength(this.element) - this.fnPaddingLength(this.element);
  }

  constructor(
    _elementRef: ElementRef,
    @Optional() @Inject(RESIZABLE_TOKEN) public parentResizable: Resizable) {
    this.element = _elementRef.nativeElement;
  }

  ngOnInit() {
    [this.fnPaddingEdgeLength, this.fnPaddingLength] = this.axis === 'x' ?
      [ClientWidth, HorizontalPadding] :
      [ClientHeight, VerticalPadding];
  }

}
