import { BsResizerTrigger } from './resizer';
import { InjectionToken } from '@angular/core';

export interface Resizable {
  _lastSize: number;
  targetSize: number;
  targetSizeCalc: string;
  minSize: number | null;
  maxSize: number | null;
  nearTriggers: [BsResizerTrigger, BsResizerTrigger];
}

export const RESIZABLE_TOKEN = new InjectionToken<Resizable>('resizable');
