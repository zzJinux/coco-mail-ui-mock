import {
  Directive,
  OnChanges,
  Input,
  ElementRef
} from '@angular/core';

@Directive({
  selector: '[matIcon]',
  host: {
    'class': 'material-icons'
  }
})
export class MatIcon implements OnChanges {

  @Input('matIcon') glyphName: string;

  constructor(private elemRef: ElementRef) { }

  ngOnChanges(ch) {
    const _old = `material-icon-${ch.glyphName.previousValue}`;
    const _new = `material-icon-${ch.glyphName.currentValue}`;
    const classList = this.elemRef.nativeElement.classList;
    classList.remove(_old);
    classList.add(_new);
  }

}
