import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BsRadioModule } from './radio';
import { BsCheckboxModule } from './checkbox';
import { BsSelectModule } from './select';
import { BsResizerModule } from './resizer';
import { BsGlyph } from './bs-glyph';
import { MatIcon } from './mat-icon';
import { BsChip } from './bs-chip';
import { BsError } from './bs-error';
import { PlaceholderDirective } from './placeholder.polyfill';

@NgModule({
  imports: [
    CommonModule,
    BsRadioModule,
    BsCheckboxModule,
    BsSelectModule,
    BsResizerModule
  ],
  declarations: [
    BsGlyph,
    MatIcon,
    BsChip,
    BsError,
    PlaceholderDirective
  ],
  exports: [
    BsGlyph,
    MatIcon,
    BsChip,
    BsError,
    PlaceholderDirective,
    BsRadioModule,
    BsCheckboxModule,
    BsSelectModule,
    BsResizerModule
  ]
})
export class CoreWidgetModule { }
