import { Component, ViewContainerRef } from '@angular/core';
import { HostlessComponentBase } from '../utils/hostless-component-base';

@Component({
  selector: 'mail-menu__tag',
  templateUrl: './tag.html',
  styleUrls: ['./tag.css'],
  preserveWhitespaces: false
})
export class MailMenu_Tag extends HostlessComponentBase {

  mailtag_list = [
    { name: 'tagtag1', color: 'crimson' },
    { name: 'tagtag2', color: 'DarkMagenta' },
    { name: 'tagtag3', color: 'green' }
  ];

  constructor(vcr: ViewContainerRef) {
    super(vcr);
  }

  openMailboxPopup() {
    console.log('call openMailboxPopup');
  }

  goRouterLink() {
    console.log('call goRouterLink');
  }

  removeMailTag() {
    console.log('call removeMailTag');
  }
}
