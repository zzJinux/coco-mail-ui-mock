import { Component, ViewChild, ViewContainerRef, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { HostlessComponentBase } from '../../utils/hostless-component-base';

@Component({
  selector: 'mail-menu__footer',
  templateUrl: './footer.html',
  styleUrls: ['./footer.css'],
  preserveWhitespaces: false
})
export class MailMenu_Footer extends HostlessComponentBase {

  @ViewChild('skinDialog') skinDialog: TemplateRef<any>;

  constructor(private modal: BsModalService, vcr: ViewContainerRef) {
    super(vcr);
  }

  openModal() {
    console.log('call openModal');
    this.modal.show(this.skinDialog, { ignoreBackdropClick: false, keyboard: true });
  }

}
