import { Component, ViewContainerRef } from '@angular/core';
import { HostlessComponentBase } from '../../utils/hostless-component-base';

@Component({
  selector: 'mail-menu__header',
  templateUrl: './header.html',
  styleUrls: ['./header.css'],
  preserveWhitespaces: false
})
export class MailMenu_Header extends HostlessComponentBase {

  menu = '메일';

  constructor(vcr: ViewContainerRef) {
    super(vcr);
  }

  clickMenu() {
    console.log('call clickMenu');
  }

  gotoMainMenu() {
    console.log('call gotoMainMenu');
  }
}