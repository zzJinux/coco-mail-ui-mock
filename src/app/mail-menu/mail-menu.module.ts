import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared.module';
import { MiddleUIModule } from '../middle-ui';

import { MailMenu } from './mail-menu';
import { MailMenu_Header } from './header/header';
import { MailMenu_Footer } from './footer/footer';
import { MailMenu_Tag } from './tag';

const DECLARATIONS = [MailMenu, MailMenu_Header, MailMenu_Footer, MailMenu_Tag];

@NgModule({
  imports: [CommonModule, SharedModule, MiddleUIModule],
  declarations: DECLARATIONS,
  exports: DECLARATIONS
})
export class MailMenuModule { }
