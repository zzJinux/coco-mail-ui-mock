import { Component, ViewEncapsulation } from '@angular/core';
import { NODES_TEST as nodes } from '../mock-data/mailbox-tree';

@Component({
  selector: 'mail-menu',
  templateUrl: './mail-menu.html',
  styleUrls: ['./mail-menu.scss'],
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false
})
export class MailMenu {
  nodes = nodes;
}
