import { Component, ViewEncapsulation, ViewChild, ElementRef, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { ContactsDialog } from './middle-ui/contacts-dialog';
import { RecentImportantDialog } from './middle-ui/recent-important-dialog';
import { NouserDialog } from './middle-ui/nouser-dialog';
import { DialogAbsent } from './mail-conf/etc/dialog-absent';
import { DialogAccount } from './mail-conf/etc/dialog-account';
import { DialogOuter } from './mail-conf/etc/dialog-outer';
import { DialogSign } from './mail-conf/etc/dialog-sign';
import { DialogFilter } from './mail-conf/mailbox/dialog-filter';
import { DialogForward } from './mail-conf/mailbox/dialog-forward';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  preserveWhitespaces: false,
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  name = 'Angular 5';

  @ViewChild('main') mainElement: ElementRef;

  rotator = 0;
  COUNT = 3;

  constructor(public modal: BsModalService) { }

  ngOnInit() {
    // this.modal.show(ContactsDialog);
    // this.modal.show(RecentImportantDialog);
    // this.modal.show(NouserDialog);
    // this.modal.show(DialogAbsent);
    // this.modal.show(DialogAccount);
    this.modal.show(DialogOuter);
    // this.modal.show(DialogSign);
    // this.modal.show(DialogFilter);
    // this.modal.show(DialogForward);
  }

  setLeftMargin(sidebarWidth: number) {
    // (<HTMLElement>this.mainElement.nativeElement).style.marginLeft = `${sidebarWidth}px`;
  }
}
